FROM nginx

RUN mkdir -p /html/default /html/updates /html/blog /html/woodwork-projects \
             /blog/generate /blog/cached /blog/content

RUN mkdir -p /blogs/blog2017 && \
    ln -s /blog/generate /blogs/blog2017/blog_lambda && \
    ln -s /blog/cached /blogs/blog2017/blog_cache && \
    ln -s /blog/content /blogs/blog2017/blog_content && \
    ln -s /blog/cached /blog/blog_cache && \
    ln -s /blog/content /blog/blog_content && \
    mkdir -p /var/data/tom && \
    ln -s /blog/generate /var/data/tom/tompaton-com-blog-generate && \
    ln -s /blog/cached /var/data/tom/tompaton-com-blog-cache && \
    ln -s /blog/content /var/data/tom/tompaton-com-blog-content && \
    ln -s /blog/cached /blog/tompaton-com-blog-cache && \
    ln -s /blog/content /blog/tompaton-com-blog-content

COPY default.conf /etc/nginx/conf.d/default.conf

COPY html /html/default

EXPOSE 80
