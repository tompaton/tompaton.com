.PHONY: build run deploy test


build:
	docker-compose build

run:
	docker-compose up


deploy: build deploy-woodwork-projects
	docker-compose push
	scp production.* phosphorus:/var/data/tompaton.com/
	ssh tom@phosphorus "cd /var/data/tompaton.com && docker-compose -f production.yml pull"


deploy-woodwork-projects:
	rsync -avz --delete html/woodwork-projects phosphorus:/var/data/tompaton.com/


deploy-updates-pages:
	scp updates/more.html phosphorus:/var/data/tompaton.com/updates/
	rsync -avz --delete updates/pages/ phosphorus:/var/data/tompaton.com/updates/pages/


test: test-200 test-404 test-modify test-new
	@echo Test OK


define CHECK_HTTP_STATUS
@xargs -n1 -P 10 \
       curl -o /dev/null --silent --head \
            --write-out '%{url_effective}: %{http_code}\n' < $<
endef

.PHONY: test-200
test-200: test-urls-200.txt
	$(CHECK_HTTP_STATUS) | grep -v ' 200$$' \
	&& echo '200 Failed!' && exit 1 \
	|| echo '200 OK'


.PHONY: test-404
test-404: test-urls-404.txt
	$(CHECK_HTTP_STATUS) | grep -v ' 404$$' \
	&& echo '404 Failed!' && exit 1 \
	|| echo '404 OK'


host=http://localhost:5510

.PHONY: test-modify
test-modify:
	@rm -f updates/index.html
	@curl --silent $(host)/index.html \
	  | grep '<h1>tompaton.com</h1>' > /dev/null \
	  || exit 1
	@curl --silent $(host)/index.html \
	  | sed 's,<h1>tompaton.com</h1>,<h1>XX tompaton.com</h1>,' \
	  > updates/index.html_
	@mv updates/index.html_ updates/index.html
	@curl --silent $(host)/index.html \
	  | grep '<h1>XX tompaton.com</h1>' > /dev/null \
	  || exit 1
	@rm -f updates/index.html
	@echo Updates - Modify OK


.PHONY: test-new
test-new:
	@rm -f updates/test-updates-new.html
	@curl -o /dev/null --silent $(host)/test-updates-new.html \
	  --write-out '%{http_code}' \
	  | grep '^404$$' > /dev/null \
	  || exit 1
	@echo 'new file' > updates/test-updates-new.html
	@curl --silent $(host)/test-updates-new.html \
	  | grep '^new file$$' > /dev/null \
	  || exit 1
	@rm -f updates/test-updates-new.html
	@echo Updates - New file OK
