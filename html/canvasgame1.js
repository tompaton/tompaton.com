/* TODO
 * fancy graphics - use bezier curves to correctly outline blobs...
 */

function initialize(id)
{
  var canvas = document.getElementById(id);
  if (canvas.getContext)
  {
    if(window.navigator.userAgent.indexOf("iPhone")!=-1||window.navigator.userAgent.indexOf("iPod")!=-1)
    {
      canvas.addEventListener('touchstart', canvas_click, false);
      if(!window.navigator.standalone) document.getElementById("addtohome").style.display = "";
    } else {
      canvas.addEventListener('click', canvas_click, false);
    }
    canvas.pieces = new Array( new Piece("#FF9900", "#B36B00", "#FFE6BF", "#FFCC80"),
			       new Piece("#0033CC", "#00248F", "#BFCFFF", "#809FFF"),
			       new Piece("#400099", "#2D006B", "#DABFFF", "#B580FF"),
			       new Piece("#FFE500", "#B3A000", "#FFF9BF", "#FFF280") );

    var tally = document.getElementById("tally").tBodies[0];
    for(var i=0; i<canvas.pieces.length; i++)
      tally.rows[0].cells[i].style.backgroundColor = canvas.pieces[i].colors[3];
  }
}

function newgame(id)
{
  var canvas = document.getElementById(id);
  if (canvas.getContext)
  {
    canvas.board = {
      rows: 8, cols: 10, cells: new Array()
    };
    canvas.height = 250;
    canvas.width = canvas.board.cols * canvas.height/canvas.board.rows;

    Piece.prototype.sx = canvas.width / canvas.board.cols;
    Piece.prototype.sy = canvas.height / canvas.board.rows;
    Piece.prototype.canvasheight = canvas.height;

    var ctx = canvas.getContext('2d');
    for(var i=0; i<canvas.pieces.length; i++)
      canvas.pieces[i].makeGrads(ctx);

    for(var i=0; i<canvas.board.cols; i++)
      canvas.board.cells.push(new Array(canvas.board.rows));

    var pool = new Array(canvas.board.rows * canvas.board.cols);
    for(var i=0; i < pool.length; i++)
      pool[i] = i % canvas.pieces.length;

    pool = shuffle(pool);

    for(var r=0; r<canvas.board.rows; r++)
      for(var c=0; c<canvas.board.cols; c++)
	canvas.board.cells[c][r] = pool.pop();

    var tally = draw(canvas);

    updateTally(tally, document.getElementById("tally"));

    document.getElementById("clicks").value = 0;
  }
}

//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/array/shuffle [v1.0]
function shuffle(o)
{ //v1.0
  for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
};

function canvas_click(e)
{
  var canvas = this;
  var pos = findPositionWithScrolling(canvas);
  var x = e.touches ? e.touches[0].pageX : e.pageX;
  var y = e.touches ? e.touches[0].pageY : e.pageY;
  var c = Math.floor((x-pos[0]) / canvas.pieces[0].sx),
      r = Math.floor((canvas.height - (y-pos[1])) / canvas.pieces[0].sy);
  if(canMove(canvas,c,r)) {
    updateClicks(1);
    processMove(canvas,c,r);
    collapse(canvas);
    var tally = draw(canvas);
    updateTally(tally, document.getElementById("tally"));
  }
}

// @http://www.howtocreate.co.uk/tutorials/javascript/browserspecific
function findPositionWithScrolling( oElement ) {
  function getNextAncestor( oElement ) {
    var actualStyle;
    if( window.getComputedStyle ) {
      actualStyle = getComputedStyle(oElement,null).position;
    } else if( oElement.currentStyle ) {
      actualStyle = oElement.currentStyle.position;
    } else {
      //fallback for browsers with low support - only reliable for inline styles
      actualStyle = oElement.style.position;
    }
    if( actualStyle == 'absolute' || actualStyle == 'fixed' ) {
      //the offsetParent of a fixed position element is null so it will stop
      return oElement.offsetParent;
    }
    return oElement.parentNode;
  }
  if( typeof( oElement.offsetParent ) != 'undefined' ) {
    var originalElement = oElement;
    for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent ) {
      posX += oElement.offsetLeft;
      posY += oElement.offsetTop;
    }
    if( !originalElement.parentNode || !originalElement.style || typeof( originalElement.scrollTop ) == 'undefined' ) {
      //older browsers cannot check element scrolling
      return [ posX, posY ];
    }
    oElement = getNextAncestor(originalElement);
    while( oElement && oElement != document.body && oElement != document.documentElement ) {
      posX -= oElement.scrollLeft;
      posY -= oElement.scrollTop;
      oElement = getNextAncestor(oElement);
    }
    return [ posX, posY ];
  } else {
    return [ oElement.x, oElement.y ];
  }
}

function draw(canvas)
{
  var ctx = canvas.getContext('2d');

  var tally = new Array(canvas.pieces.length);
  tally.connected = new Array(canvas.pieces.length);
  for(var i=0; i<tally.length; i++)
  {
    tally[i] = 0;
    tally.connected[i] = 0;
  }
  ctx.clearRect(0,0,canvas.width,canvas.height);

  for(var r=0; r<canvas.board.rows; r++)
    for(var c=0; c<canvas.board.cols; c++)
    {
      var p = canvas.board.cells[c][r];
      if(p != null)
      {
	var con = getConnections(canvas, c, r);
	canvas.pieces[p].draw(ctx, c, r, con);
	tally[p] += 1;
	tally.connected[p] += (con.left||con.up) ? 1 : 0;
      }
    }

  return tally;
}

function updateTally(tally, table)
{
  var sum = 0, con = 0;
  for(var i=0; i<tally.length; i++)
  {
    var c = table.tBodies[0].rows[0].cells[i];
    c.replaceChild(document.createTextNode(tally[i]), c.lastChild);
    sum += tally[i];
    con += tally.connected[i];
  }
  var c = table.tFoot.rows[0].cells[0];
  c.replaceChild(document.createTextNode(sum), c.lastChild);

  if(con > 0)
    c.style.backgroundColor = "#8f8";
  else {
    c.style.backgroundColor = "#f44";
    updateClicks(sum);
    updateHistory(1*document.getElementById("clicks").value);
    updateHistogram(1*document.getElementById("clicks").value);
  }
}

function Piece(color1, color2, color3, color4)
{
  this.colors = [color1, color2, color3, color4];
}

Piece.prototype.makeGrads = function(ctx)
{
  function addColorStops(p, g) {
    g.addColorStop(0, p.colors[2]);
    g.addColorStop(0.1, p.colors[3]);
    g.addColorStop(0.8, p.colors[0]);
    g.addColorStop(1, p.colors[1]);
  }

  this.gradient = ctx.createRadialGradient(this.sx/3,this.sx/3,this.sx/12,this.sx/2,this.sy/2,0.9*this.sx/2);
  addColorStops(this, this.gradient);

  this.gradienth = ctx.createLinearGradient(0,this.sy/4,0,3*this.sy/4);
  addColorStops(this, this.gradienth);

  this.gradientv = ctx.createLinearGradient(this.sx/4,0,3*this.sx/4,0);
  addColorStops(this, this.gradientv);
}

Piece.prototype.draw = function(ctx, x, y, connected)
{
  ctx.save();
  ctx.translate(x*this.sx, this.canvasheight-(y+1)*this.sy);

  ctx.globalCompositeOperation = "destination-over";

  if(connected.left)
  {
    ctx.fillStyle = this.gradienth;
    ctx.fillRect(-this.sx/2,this.sy/2-this.sy/4,this.sx,this.sy/2);
  }
  if(connected.up)
  {
    ctx.fillStyle = this.gradientv;
    ctx.fillRect(this.sx/2-this.sx/4,this.sy/2,this.sx/2,this.sy);
  }
  if(connected.block)
  {
    ctx.fillStyle = this.colors[1];
    ctx.fillRect(-this.sx/3,this.sy-this.sy/3,2*this.sx/3,2*this.sy/3);
  }

  ctx.fillStyle = this.gradient;
  ctx.globalCompositeOperation = "source-over";
  ctx.beginPath();
  ctx.arc(this.sx/2,this.sy/2,0.9*this.sx/2,0,Math.PI*2,true);
  ctx.fill();

  ctx.restore();
}

function getConnections(canvas, c, r)
{
  var up = false, left = false, block = false;

  if(c>0 && canvas.board.cells[c][r]==canvas.board.cells[c-1][r]) left = true;
  if(r>0 && canvas.board.cells[c][r]==canvas.board.cells[c][r-1]) up = true;

  if(up && left && canvas.board.cells[c][r]==canvas.board.cells[c-1][r-1]) block = true;

  return { up: up, left: left, block: block };
}

function canMove(canvas, c, r)
{
  var p = canvas.board.cells[c][r];

  if(p == null) return false;

  return (c > 0 && canvas.board.cells[c-1][r]==p
	  || r > 0 && canvas.board.cells[c][r-1]==p
	  || c+1 < canvas.board.cols && canvas.board.cells[c+1][r]==p
	  || r+1 < canvas.board.rows && canvas.board.cells[c][r+1]==p);
}

function processMove(canvas, c, r)
{
  var p = canvas.board.cells[c][r];
  canvas.board.cells[c][r] = null;

  if(c > 0 && canvas.board.cells[c-1][r]==p) processMove(canvas, c-1, r);
  if(r > 0 && canvas.board.cells[c][r-1]==p) processMove(canvas, c, r-1);
  if(c+1 < canvas.board.cols && canvas.board.cells[c+1][r]==p) processMove(canvas, c+1, r);
  if(r+1 < canvas.board.rows && canvas.board.cells[c][r+1]==p) processMove(canvas, c, r+1);
}

function collapse(canvas)
{
  for(var c=0; c < canvas.board.cols; c++)
  {
    for(var r=0, b=0; r < canvas.board.rows; r++)
      if(canvas.board.cells[c][r] != null)
	canvas.board.cells[c][b++] = canvas.board.cells[c][r];
    for(;b<canvas.board.rows;b++)
      canvas.board.cells[c][b] = null;
  }

  for(var c=0, b=0; c < canvas.board.cols; c++)
    if(canvas.board.cells[c][0] != null)
    {
      for(var r=0; r<canvas.board.rows; r++)
	canvas.board.cells[b][r] = canvas.board.cells[c][r];
      b++;
    }
  for(;b<canvas.board.cols;b++)
    for(var r=0; r<canvas.board.rows; r++)
      canvas.board.cells[b][r] = null;
}

function updateClicks(clicks)
{
  document.getElementById("clicks").value = 1*document.getElementById("clicks").value + clicks;
}

function updateHistory(clicks)
{
  var hist = document.getElementById("history");
  var row = document.createElement("div");
  row.style.fontSize = "0.8em";
  row.style.border = "1px solid gray";
  row.style.lineHeight = "1em; overflow: hidden";
  row.style.width = 0.25*clicks+"em";
  row.appendChild(document.createTextNode(clicks));
  hist.insertBefore(row, hist.firstChild);

  // update history colours
  var min = 1000, max = 0, sum = 0;
  for(var i=0; i<hist.childNodes.length; i++)
  {
    var s = 1*hist.childNodes[i].textContent;
    min = min < s ? min : s;
    max = max > s ? max : s;
    sum += s;
  }
  var avg = sum/hist.childNodes.length;

  for(var i=0; i<hist.childNodes.length; i++)
  {
    var s = 1*hist.childNodes[i].textContent;
    if(s>=avg) {
      var p = (s-avg)/(1+max-avg);
      hist.childNodes[i].style.backgroundColor = "rgb(255,"+Math.floor(255*(1-p))+",0)";
    } else {
      var p = (s-min)/(1+avg-min);
      hist.childNodes[i].style.backgroundColor = "rgb("+Math.floor(255*p)+",255,0)";
    }
  }
}


function updateHistogram(clicks)
{
  var row = document.createElement("div");
  row.style.border = "1px solid gray";
  row.style.backgroundColor = "silver";
  row.style.width = "0.25em";
  row.style.height = "0.25em";

  var tbl = document.getElementById("histogram");
  var c = tbl.tBodies[0].rows[0].cells[clicks-1];
  c.insertBefore(row, c.firstChild);
}