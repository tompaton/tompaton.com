#!/usr/local/bin/perl -I./cgi-bin
##########################################################################################

# perl -w genchild.pl index.xml
# >> Generates a file for each <section> in the <webpage>
# >> section file is named with same convention as anchors (ie menuid or title)

use XML::Parser;
use Webpage;
use strict;

my $xmlfile = "index.xml";		# for command line source file

die "Content-type: text/plain\n\nCan't find input file \"$xmlfile\"" unless -f $xmlfile;

my $parser = new XML::Parser(Style => 'objects', Pkg => 'Webpage', ErrorContext => 2);

my @parsetree = $parser->parsefile($xmlfile);

my $webpage = $parsetree[0][0];

if($webpage->rvalidate(\&error_handler))
{
	$webpage->output();
}
##########################################################################################
sub error_handler
{
	my $errstr = shift;
	print "ERROR: $errstr\n";
}
