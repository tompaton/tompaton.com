#!/usr/local/bin/perl -I./cgi-bin
##########################################################################################

use strict;
use HTTP::Status;
use HTTP::Response;
use LWP::UserAgent;
use URI::URL;
use HTML::TokeParser;

# get todays date
my @months = ("January","February","March","April","May","June","July",
      "August","September","October","November","December");
my @days = ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
my @daysinmonth = (31,28,31,30,31,30,31,31,30,31,30,31);

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
my $curdate = "$days[$wday], $mday $months[$mon] $year";

open(OUTPUT,">digest.html");
my $output = \*OUTPUT;

print $output "<html><head><title>Tom Paton's Homepage - WebDigest</title>";

#		open(BOILERPLATE,"<child_pre.html");
#		while(<BOILERPLATE>) { print $output $_; }
#		close(BOILERPLATE);
print $output "\n<!-- BEGIN CONTENT	-->\n";
print $output "<h1>Tom's WebDigest</h1><h3>$curdate</h3>\n";

print $output "<hr>\n";
Item1($output);
print $output "<hr>\n";
Item2($output);
print $output "<hr>\n";
Item3($output);
print $output "<hr>\n";
Item4($output);
print $output "<hr>\n";
Item5($output);
print $output "<hr>\n";
Item6($output);
print $output "<hr>\n";


print $output "<!-- END CONTENT -->\n";
#		open(BOILERPLATE,"<child_post.html");
#		while(<BOILERPLATE>) { print $output $_; }
#		close(BOILERPLATE);
close(OUTPUT);

sub Item1
{
	my $output = shift;
	print $output "<h2>MetaCreations Image of the Day</h2>\n";

	# get first IOTD page
	my ($code, $type, $data)  = get_html("http://www.fractal.com/galleries/iotd/");
	return if (not_good($output, $code, $type, "http://www.fractal.com/galleries/iotd/"));

	# skip through and grab URL for todays page
	my $url = "";
	my $p = HTML::TokeParser->new(\$data);
	AHREF: while (my $token = $p->get_tag("a"))
	{
		$url = globalize_url($token->[1]{href}, "http://www.fractal.com/galleries/iotd/");
		if(my $text = $p->get_trimmed_text("/a"))
		{
			last AHREF if( $text =~ /today's IOTD/);
		}
	}

	# now get next page
	($code, $type, $data)  = get_html($url);

	if (not not_good($output, $code, $type, $url))
	{
		# look for table data <TD> sections
		$p = HTML::TokeParser->new(\$data);

		my $td_1 = " ";
		my $td_2 = " ";

		my $found_img = 0;

		TD: while ($p->get_tag("td"))
		{
			my $td = " ";
			TOKEN: while (my $token = $p->get_token())
			{
				# $Token - reference to ["S", $tag, %$attr, @$attrseq, $origtext]
				#					or  ["E", $tag, $origtext]
				#					or  ["T", $text] or ["C", $text] or ["D", $text])

				SWITCH: {
					if ($token->[1] eq "td") {
						# new TD or end /TD tag, so we're done in the TD
						last TOKEN if($token->[0] eq "S" or $token->[0] eq "E");
					}
					if ($token->[0] eq "S" and $token->[1] eq "img") {
						# we want the IMG tag
						# need to put in the full url for the image path

						if(defined $token->[2]{alt} and $token->[2]{alt} eq "Image of the Day")
						{
							if(not $token->[2]{src} =~ /iotd_header\.gif/i)
							{
								my $src = globalize_url($token->[2]{src}, $url);
								$td .= "<img src=\"$src\" width=$token->[2]{width} height=$token->[2]{height} alt=\"Image of the Day\">";
								$found_img = 1;
							}
						}

						last SWITCH;
					}
					if ($token->[1] eq "p" or $token->[1] eq "b" or $token->[1] eq "i") {
						# P, B, I formatting tags
						$td .= "<$token->[1]>", last SWITCH if($token->[0] eq "S");
						$td .= "</$token->[1]>", last SWITCH if($token->[0] eq "E");
					}
					if ($token->[1] eq "br") {
						# convert BR to spaces
						$td .= " ", last SWITCH if($token->[0] eq "S" or $token->[0] eq "E");
					}
					if ($token->[0] eq "T") {
						# we want the text
						$td .= $token->[1];
						last SWITCH;
					}
					# ignore other tags
				}
			}
			$td_1 = $td_2;
			$td_2 = $td;

			# if last one has image in it, we're done
			last TD if ($found_img == 1);
		}
		# remove extra whitespace
		$td_1 =~ s/[\s]+/ /gi;

		# put image at top
		print $output "<table width=\"100%\" border=0><tr><td>$td_2</td><td>$td_1</td></tr></table>\n";
	}
}
sub Item2
{
	my $output = shift;

	print $output "<h2>Triple J Tours</h2>\n";

#	my $url = "file://localhost/D:/docs/homepage/Digest/Pages/Triple J Tours.htm";
	my $url = "http://www.abc.net.au/triplej/tours.htm";
	# now get page
	my ($code, $type, $data)  = get_html($url);

	if (not not_good($output, $code, $type, $url))
	{
		my $p = HTML::TokeParser->new(\$data);

		while (my $token = $p->get_tag("a")) {
			if( defined $token->[1]{href} )
			{
		    	my $link = globalize_url($token->[1]{href},$url);
		    	my $text = $p->get_trimmed_text("/a");
		    	print $output "<a href=\"$link\">$text</a> | \n";
			}
		}
  	}
}
sub Item3
{
	my $output = shift;

	print $output "<h2>Item 3</h2>\n";
}
sub Item4
{
	my $output = shift;

	print $output "<h2>Item 4</h2>\n";
}
sub Item5
{
	my $output = shift;

	print $output "<h2>Item 5</h2>\n";
}
sub Item6
{
	my $output = shift;

	print $output "<h2>Item 6</h2>\n";
}


sub get_html() {
  my($url, $proxy) = @_;

# Create a User Agent object
  my $ua = new LWP::UserAgent;
  $ua->agent("tomsdigest/1.0");

# If proxy server specified, define it in the User Agent object
  if (defined $proxy) {
    my $proxy_url = new URI::URL $url;
    $ua->proxy($proxy_url->scheme, $proxy);
  }

# Ask the User Agent object to request a URL.
# Results go into the response object (HTTP::Reponse).

  my $request = new HTTP::Request('GET', $url);
  my $response = $ua->request($request);

  return ($response->code, $response->content_type, $response->content);
}

# returns 1 if the request was not OK or HTML, else 0

sub not_good {
  my ($output, $code, $type, $url) = @_;

  if ($code != RC_OK) {
  	print $output "<p>ERROR: $url had response code of $code</p>\n";
    return 1;
  }

  if ($type !~ m@text/html@) {
  	print $output "<p>ERROR: $url is not HTML.</p>\n";
    return 1;
  }
  return 0;
}

sub globalize_url() {

  my ($partial, $model) = @_;
  my $url = new URI::URL($partial, $model);
  my $globalized = $url->abs->as_string;

  return $globalized;
}

