#!/usr/local/bin/perl
##########################################################################################
use CGI;
use strict;
use Fcntl ':flock'; # import LOCK_* constants
##########################################################################################
# what file are we looking for?
my $sourcefile=$ENV{PATH_INFO};
$sourcefile =~ s|~madmick|www/users/madmick|g;	# translate /~madmick/ to /www/users/madmick/
##########################################################################################
print "Content-type: text/html\n\n";
open(HTML,$sourcefile);
print while <HTML>;
close(HTML);
##########################################################################################
# check browser type
my $realbrowser = $ENV{HTTP_USER_AGENT};
##########################################################################################
# check if this hit resulted from a search engine query
my $refer = $ENV{HTTP_REFERER};
# split query parameters off the end
my ($refersite,$referquery) = split( /\?/, $refer);
# the parameter we are interested in varies depending on the site
my $referengine = 'Nosearch';
if( $referquery gt " ")
{
	if( $refersite =~ /yahoo/i ) { $referengine = 'Yahoo'; }
	elsif( $refersite =~ /excite/i ) { $referengine = 'Excite'; }
	elsif( $refersite =~ /ultraseek/i ) { $referengine = 'Ultraseek'; }
	elsif( $refersite =~ /altavista/i ) { $referengine = 'Altavista'; }
	elsif( $refersite =~ /alta-vista/i ) { $referengine = 'Altavista'; }
	elsif( $refersite =~ /anzwers/i ) { $referengine = 'Anzwers'; }
	elsif( $refersite =~ /google/i ) { $referengine = 'Google'; }
	elsif( $refersite =~ /hotbot/i ) { $referengine = 'HotBot'; }
	elsif( $refersite =~ /fireball/i ) { $referengine = 'FireBall'; }
	elsif( $refersite =~ /search\.cnet/i ) { $referengine = 'CNet'; }
	elsif( $refersite =~ /iltrovatore/i ) { $referengine = 'Iltrovatore'; }
	elsif( $refersite =~ /search\.go2net/i ) { $referengine = 'Go2Net'; }
	elsif( $refersite =~ /search\.ninemsn/i ) { $referengine = 'NineMSN'; }
	elsif( $refersite =~ /search\.msn/i ) { $referengine = 'MSN'; }
}
##########################################################################################
my @months = ("January","February","March","April","May","June","July",
              "August","September","October","November","December");
my @days = ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
my $curdate = "$days[$wday], $mday $months[$mon] $year";
my $cdate = sprintf("%04d%02d%02d",$year,$mon,$mday);
##########################################################################################
# count based on requested $xmlfile, $browser, source location, user name etc...
my %stats;
my $count;
my $file;
my $hitcount;

open(STATS, "</www/users/madmick/cgi-bin/count.dat");
# lock file
flock(STATS,LOCK_EX);
# read current value
while(<STATS>)
{
	chop;
	($file,$count) = split(/=/,$_);
	$stats{$file} = $count;
}
close(STATS);

# increment values
$stats{"FILE  : $sourcefile"} += 1;
$stats{"TOTAL : "} += 1;
$stats{"CLIENT: $realbrowser"} += 1;
$stats{"DATE  : $cdate $curdate"} += 1;
my $query = $ENV{QUERY_STRING};
$query =~ s/=/--/g;
$query =~ s/:/../g;
$stats{"QUERY : $query"} += 1;
$stats{"HOST  : $ENV{REMOTE_HOST}"} += 1;
# $refer is set above
$refer =~ s/=/--/g;		# can't have : or = in there...
$refer =~ s/:/../g;
if(lc $referengine ne "nosearch")
{
	$stats{"REFERS: $refer"} += 1;
} else
{
	$stats{"REFER : $refer"} += 1;
}
$stats{"SEARCH: $referengine"} += 1;

my $kw = "(none)";
# split query parameters off the end
my ($site,$query) = split( /\?/, lc $ENV{HTTP_REFERER});

# fire up the CGI module
my $cgi = new CGI($query);

# get the value of the parameter
if( $referengine eq "Yahoo" ) { $kw = $cgi->param('p'); }
elsif( $referengine eq "Excite" ) { $kw = $cgi->param('search'); }
elsif( $referengine eq "Ultraseek" ) { $kw = $cgi->param('qt'); }
elsif( $referengine eq "Altavista" ) { $kw = $cgi->param('q'); }
elsif( $referengine eq "Anzwers" ) { $kw = $cgi->param('query'); }
elsif( $referengine eq "Google" ) { $kw = $cgi->param('q'); }
elsif( $referengine eq "HotBot" ) { $kw = $cgi->param('MT'); }
elsif( $referengine eq "FireBall" ) { $kw = $cgi->param('q'); }
elsif( $referengine eq "NineMSN" ) { $kw = $cgi->param('MT'); }
elsif( $referengine eq "MSN" ) { $kw = $cgi->param('MT'); }
elsif( $referengine eq "Go2Net" ) { $kw = $cgi->param('general'); }
elsif( $referengine eq "Iltrovatore" ) { $kw = $cgi->param('what'); }
elsif( $referengine eq "CNet" ) { $kw = $cgi->param('query'); }

# remove any quotes or + signs
$kw =~ s/"//g; $kw =~ s/\+//g;

$stats{"KEYWRD: $kw --- $sourcefile"} += 1;

# log misc http_ headers (cookies and such...)
my $variable;
foreach $variable ( keys %ENV )
{
	if($variable=~/HTTP_/i)
	{
		if( $variable ne "HTTP_REFERER"
			and $variable ne "HTTP_ACCEPT"
			and $variable ne "HTTP_USER_AGENT" )
		{
			my $http = $ENV{$variable};
			$http =~ s/=/--/g;
			$http =~ s/:/../g;
			$stats{"HTTP  : $variable--$http"}+=1;
		}
	}
}

# write hitcount values to file
open(STATS, ">/www/users/madmick/cgi-bin/count.dat");
foreach (keys %stats)
{
	print STATS "$_=$stats{$_}\n";
}
# unlock file
flock(STATS,LOCK_UN);

close(STATS);
##########################################################################################
