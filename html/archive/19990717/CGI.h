#ifndef CGI_H
#define CGI_H

#include <stdlib.h>
#include <string>

class CGI {
	private:
		

	public:
			// exception class to give an error if the environment variable is not
			// available from the server.
		class NotAvailable {};
			// this enumeration defines the variables that may be accessed
			// from the server.
		enum EnvVar {	
			evServer_Software, 
			evServer_Name, 
			evGateway_Interface, 
			evServer_Protocol, 
			evServer_Port, 
			evRequest_Method,
			evPath_Info, 
			evPath_Translated, 
			evScript_Name,
			evQuery_String, 
			evRemote_Host, 
			evRemote_Addr,
			evAuth_Type, 
			evRemote_User, 
			evRemote_Ident,
			evContent_Type, 
			evContent_Length, 
			evHTTP_Accept,
			evHTTP_User_Agent
		};

		string getEnvVar( EnvVar what ) {
			char * ret = getenv(getEnvVar_name(what).c_str());

			if(!ret)
				throw NotAvailable();
			else
				return ret;
		};

		string getEnvVar_name( EnvVar what ) {
			switch(what)
			{
				case evServer_Software:		return "SERVER_SOFTWARE";
				case evServer_Name: 		return "SERVER_NAME";
				case evGateway_Interface:	return "GATEWAY_INTERFACE";
				case evServer_Protocol: 	return "SERVER_PROTOCOL";
				case evServer_Port:		return "SERVER_PORT";
				case evRequest_Method:		return "REQUEST_METHOD";
				case evPath_Info: 		return "PATH_INFO";
				case evPath_Translated: 	return "PATH_TRANSLATED";
				case evScript_Name:		return "SCRIPT_NAME";
				case evQuery_String: 		return "QUERY_STRING";
				case evRemote_Host: 		return "REMOTE_HOST";
				case evRemote_Addr:		return "REMOTE_ADDR";
				case evAuth_Type: 		return "AUTH_TYPE";
				case evRemote_User: 		return "REMOTE_USER";
				case evRemote_Ident:		return "REMOTE_IDENT";
				case evContent_Type: 		return "CONTENT_TYPE";
				case evContent_Length: 		return "CONTENT_LENGTH";
				case evHTTP_Accept:		return "HTTP_ACCEPT";
				case evHTTP_User_Agent:		return "HTTP_USER_AGENT";
				default:
								return "\0";
			};
		};
};

#endif CGI_H

