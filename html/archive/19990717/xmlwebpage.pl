#!/usr/local/bin/perl
##########################################################################################

use CGI;		# load CGI routines
use XML::Parser;
use strict;

srand();

my $xmlfile = shift;		# for command line source file
my $css = shift;			# "css" or "nocss"
my $referengine = shift;		# Nosearch, Yahoo, Excite, Google, Altavista, Ultraseek, etc.

die "Content-type: text/html\n\nCan't find input file \"$xmlfile\"" unless -f $xmlfile;

my $count = 0;		# number of links
my $title = "";		# page title
my $author = "";	# author

my $inlink = 0;		# in a <LINK> (for <TITLE>, <URL>)

my $urlnum = 0;		# number of url's for artist, disc

my $link_title="";	# <TITLE> of <LINK>
my $link_url="";	# <URL> of <LINK>
my @links = ();		# <LINK>'s of <SUBSECTION>

my @sections = ();	# <SECTION>'s for menu

my $basesection="";
my $baseanchor="top";	# used for search results links to desc/subsection titles etc.
my $currentanchor="top";
my $subsectionnum=0;

my @hitpicks = ();	# hitpicks's for intro
my $ishit = 0;		# is the current <LINK> a hitpick?

my @whatsnew = ();	# links added within the last 7 days
my $isnew = 0;

my @referkeywords = keywords($referengine);		# keywords from search engine query
my @searchresults = ();						# links that match some of the keywords in the query from a search engine
my $issearchresult = 0;
my $showsearchresults = 0;					# show "Search Results" section

	# flags for ``features''
my $showfavourites = 0;	# show "Currently Enjoying" (status="hit")
my $showrecent = 0;		# show "Added in the last week" (date="...")
my $showhitcount = 0;	# show "xxx hits at dd/mm/..."
my $showlinkcount = 0;	# show "You have a choice of xxx links..."
my $showmenu = 1;		# show the section menu under each section?


# get modified date and todays date
	my @months = ("January","February","March","April","May","June","July",
	      "August","September","October","November","December");
	my @days = ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	my @daysinmonth = (31,28,31,30,31,30,31,31,30,31,30,31);

	my $mtime = (stat("$xmlfile"))[9];
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($mtime);
	$year += 1900;
	my $moddate ="$days[$wday], $mday $months[$mon] $year";

	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	$year += 1900;
	my $curdate = "$days[$wday], $mday $months[$mon] $year";

	$mday -= 7;
	if($mday <1) { $mday += $daysinmonth[$mon]; $mon --; }
	if($mon < 0) { $mon = 11; $year --; }

	my $olddate = sprintf("%4d%02d%02d\n",$year,$mon+1,$mday);


my $output = "";	# output (whole page)

my $parser = new XML::Parser(Style => 'subs', ErrorContext => 2);
$parser->setHandlers(Start => \&handle_start);
$parser->setHandlers(Entity => \&handle_entity);
$parser->setHandlers(Char => \&handle_char);
$parser->setHandlers(End => \&handle_end);
$parser->setHandlers(Comment => \&handle_comments);

$parser->parsefile($xmlfile);

my $i;

# make a menu (table) to be inserted under each <SECTION>
my $menu .= "";
if($showmenu == 1)
{
	$menu .= "\t<div class=\"menu\">\n";
#	$menu .= "\t\t<table border=0 width=\"100%\">\n";
#	$menu .= "\t\t\t<tr>\n";
#	$menu .= "\t\t\t\t<td align=center>";
	$menu .= ".:. <a href=\"#top\">Top</a> .:. ";
#	$menu .= "</td>\n";
	for $i (0 .. $#sections)
	{
#		$menu .= "\t\t\t\t<td>";
		$menu .= "<a href=\"#$sections[$i][0]\">$sections[$i][1]</a>";
#		$menu .= "</td>\n";
		$menu .= " .:. \n";
	}
#	$menu .= "\t\t\t</tr>\n";
#	$menu .= "\t\t</table>\n";
	$menu .= "\t</div>\n";

# The following code will place the menu above the section heading and simply seperate each menu entry with
# a <br>.  This is to be used with a cascading style sheet that floats the .menu to the right...
#	$menu .= "\t<div class=\"menu\">\n\t\t<a href=\"#top\">Top</a>\n";
#	for $i (0 .. $#sections)
#	{
#		$menu .= "\t\t\t\t<br><a href=\"#$sections[$i][0]\">$sections[$i][1]</a>\n";
#	}
#	$menu .= "\t</div>\n";
}
	# substitute
$output =~ s/XX-MENU-XX/$menu/g;

# make a list of hitpicks
	if($showfavourites == 1)
	{
		my $hits .= "\t\t\t<a href=\"$hitpicks[0][0]\">$hitpicks[0][1]</a>\n";

		for $i (1 .. $#hitpicks)
		{
			$hits .= "\t\t\t~ <a href=\"$hitpicks[$i][0]\">$hitpicks[$i][1]</a>\n";
		}
			# substitute
		$output =~ s/XX-HITS-XX/$hits/g;
	}
# make a list of new links
	if($showrecent == 1)
	{
		my $newlinks = "";
		if($#whatsnew)
		{
			$newlinks .= "\t\t\t<a href=\"$whatsnew[0][0]\">$whatsnew[0][1]</a>\n";
		}

		for $i (1 .. $#whatsnew)
		{
			$newlinks .= "\t\t\t~ <a href=\"$whatsnew[$i][0]\">$whatsnew[$i][1]</a>\n";
		}
			# substitute
		$output =~ s/XX-WHATSNEW-XX/$newlinks/g;
	}

# make a list of "Search Results"
	if($showsearchresults == 1)
	{
		my $results = "";
		if($#searchresults)
		{
			$results .= "\t\t\t<a href=\"$searchresults[0][0]\">$searchresults[0][1]</a>\n";
		}

		for $i (1 .. $#searchresults)
		{
			$results .= "\t\t\t~ <a href=\"$searchresults[$i][0]\">$searchresults[$i][1]</a>\n";
		}

#		if(not $results gt " ")
#		{
#			$results = "\t\t\t<i>none found</i> (Maybe you could try my <a href=\"http://yoyo.cc.monash.edu.au/~madmick/#Music\">Music</a> section?).\n";
#		}
			# substitute
		$output =~ s/XX-RESULTS-XX/$results/g;
	}

# spit out the page
print "Content-type: text/html\n\n";
print $output;

##########################################################################################
sub handle_start
{
	my $p = shift;
	my $el = shift;
	my %attribs = @_;

# <WEBPAGE>
	if ($el =~ /\bwebpage\b/i)
	{
		$title = "Untitled";
		$author = "Unknown";
		my $stylesheet = " ";
		my $rand = " ";
		my $description = " ";
		my $keywords = " ";

		foreach my $attrib (keys(%attribs))
		{
#			print " $attrib = \"$attribs{$attrib}\"\n";

			if( lc $attrib eq "title" )
			{
				$title = $attribs{$attrib};
			}
			elsif( lc $attrib eq "author" )
			{
				$author = $attribs{$attrib};
			}
			elsif( lc $attrib eq "showhitcount" )
			{
				if(lc $attribs{$attrib} eq "yes")
				{
					$showhitcount = 1;
				}
			}
			elsif( lc $attrib eq "showlinkcount" )
			{
				if(lc $attribs{$attrib} eq "yes")
				{
					$showlinkcount = 1;
				}
			}
			elsif( lc $attrib eq "stylesheet"  && $css eq "css")
			{
				$stylesheet = $attribs{$attrib};
				if( $stylesheet gt "")
				{
					$rand = $attribs{'rand'};
					if($rand gt "0")
					{
						$stylesheet .= int(rand()*$rand);
					}
					$stylesheet = "<LINK rel=\"stylesheet\" type=\"text/css\" name=\"$stylesheet\" href=\"$stylesheet.css\">\n";
				}
			}
			elsif( lc $attrib eq "description" )
			{
				if( $attribs{$attrib} gt "")
				{
					$description = "<meta name=\"description\" content=\"$attribs{$attrib}\">\n";
				}
			}
			elsif( lc $attrib eq "keywords" )
			{
				if( $attribs{$attrib} gt "")
				{
					$description = "<meta name=\"keywords\" content=\"$attribs{$attrib}\">\n";
				}
			}
			elsif( lc $attrib eq "menu" )
			{
				$showmenu = 0;
				if(lc $attribs{$attrib} eq "yes")
				{
					$showmenu = 1;
				}
			}
		}


		$output .= "<html><head>\n";
		$output .= "<title>$title</title>\n";
		$output .= "<meta name=\"author\" content=\"$author\">\n";
		$output .= $description;
		$output .= $stylesheet;
		if($css eq "nocss")	# put the background image etc in the body tag for no style sheets
		{
			$output .= "</head><body background=\"bg1_0.gif\" color=\"black\" alink=\"navy\" vlink=\"navy\" alink=\"blue\">\n";
		} else
		{
			$output .= "</head><body>\n";
		}


# <SUBSECTION>
	} elsif ($el =~ /\bsubsection\b/i)
	{
		$subsectionnum ++;
		$currentanchor = "$baseanchor$subsectionnum";
		@links = ();
		$output .= "\t<h3><a name=\"$currentanchor\">$attribs{'title'}</a></h3>\n";

		foreach my $kw (@referkeywords)
		{
			if( $attribs{'title'} =~ /$kw/i )
			{
				$issearchresult = 1;
			}
		}

		if($issearchresult)
		{
			@searchresults = (@searchresults, ["http://yoyo.cc.monash.edu.au/~madmick/index.xml#$currentanchor", "$basesection - $attribs{'title'}"]);
		}

		$issearchresult = 0;

# <SECTION>
	} elsif ($el =~ /\bsection\b/i)
	{
		my $sectitle = $attribs{'title'};
		my $menuid = $sectitle;
		my $menu   = $sectitle;

		foreach my $attrib (keys(%attribs)) {
			if( lc $attrib eq "menuid" )
			{	$menuid = $attribs{$attrib}; }
			elsif( lc $attrib eq "menu" )
			{	$menu = $attribs{$attrib}; }

		}

#		$output .= "XX-MENU-XX";	# output above for floating .menu's
		$output .= "\n\n<h2><a name=\"$menuid\">$sectitle</a></h2>\n";
		$output .= "XX-MENU-XX\n";
		$output .= "<div class=\"section\">\n";

		@sections = (@sections, [$menuid, $menu]);

		$subsectionnum = 0;
		$baseanchor = $menuid;
		$basesection = $menu;
		$currentanchor = $menuid;

		foreach my $kw (@referkeywords)
		{
			if( $sectitle =~ /$kw/i )
			{
				$issearchresult = 1;
			}
		}

		if($issearchresult)
		{
			@searchresults = (@searchresults, ["http://yoyo.cc.monash.edu.au/~madmick/index.xml#$currentanchor", $sectitle]);
		}

		$issearchresult = 0;
# <INTRO>
	} elsif ($el =~ /\bintro\b/i)
	{
		$output .= "<h1 id=\"top\">$title</h1>\n";
		$output .= "XX-MENU-XX";
		$output .= "<div class=\"section\"\n";
#		$output .= "XX-MENU-XX";
		$output .= "\t<div class=\"intro\">\n";

# <CDS>
	} elsif ($el =~ /\bcds\b/i)
	{
		$output .= "\t<h3>$attribs{'title'}</h3>\n";
		$output .= "\t<div class=\"subsection\">\n";

# <ARTIST>
	} elsif ($el =~ /\bartist\b/i)
	{
		$urlnum = 0;

		$output .= "\t\t| <b>$attribs{'name'}</b> <font size=-1>\n";

		my $link_title = lc $attribs{'name'};
		foreach my $kw (@referkeywords)
		{
			if( $link_title =~ /$kw/i )
			{
				$issearchresult = 1;
			}
		}

		if($issearchresult == 1)
		{
			@searchresults = (@searchresults, ['http://yoyo.cc.monash.edu.au/~madmick/#Music', 'My CD Collection']);
		}

		$issearchresult = 0;

# <DISC>
	} elsif ($el =~ /\bdisc\b/i)
	{
		$urlnum = 0;

		$output .= "\t\t\t-- $attribs{'title'}\n";

		my $link_title = lc $attribs{'title'};
		foreach my $kw (@referkeywords)
		{
			if( $link_title =~ /$kw/i )
			{
				$issearchresult = 1;
			}
		}

		if($issearchresult == 1)
		{
			@searchresults = (@searchresults, ['http://yoyo.cc.monash.edu.au/~madmick/#Music', 'My CD Collection']);
		}

		$issearchresult = 0;

# <DESC>
	} elsif ($el =~ /\bdesc\b/i)
	{
		$output .= "\t<p class=\"desc\">";

# <LINK>
	} elsif ($el =~ /\blink\b/i)
	{
		$link_url = "";
		$link_title = "";
		$ishit = 0;
		$isnew = 0;
		$issearchresult = 0;

		foreach my $attrib (keys(%attribs)) {
			if( lc $attrib eq "status" )
			{	if(lc $attribs{$attrib} eq "hit")
				{	$ishit = 1; } }
			if( lc $attrib eq "date" )
			{	if($attribs{$attrib} gt $olddate)
				{	$isnew = 1; } }
		}
# <JOB>
	} elsif ($el =~ /\bjob\b/i)
	{
		$output .= "\t<h3>$attribs{'title'}</h3>\n";
		$output .= "\t<div class=\"subsection\">\n";

# tags we don't want to print
# TITLE, URL, NAME, RAW, RECENT, SEARCHRESULTS, QUICKSEARCH, FAVOURITES
	} elsif ($el =~ /\burl\b/i)
	{
	} elsif ($el =~ /\btitle\b/i)
	{
	} elsif ($el =~ /\bname\b/i)
	{
	} elsif ($el =~ /\braw\b/i)
	{
	} elsif ($el =~ /\brecent\b/i)
	{
	} elsif ($el =~ /\bfavourites\b/i)
	{
	} elsif ($el =~ /\bquicksearch\b/i)
	{
	} elsif ($el =~ /\bsearchresults\b/i)
	{

# HTML tags
	} else
	{
		$output .= "<$el";
		foreach my $attrib (keys(%attribs)) {
			$output .= " $attrib = \"$attribs{$attrib}\"";
		}
		$output .= ">";
	}
}
##########################################################################################
sub handle_entity
{
	my ($xp, $name, $val, $sysid, $pubid, $ndata) = @_;
	if ($xp->current_element eq 'url')
	{
		$link_url .= $val;
	}
}

##########################################################################################
sub handle_char
{
	my ($p, $data) = @_;
	return if ($data =~ /^\s*$/m);

#<URL></URL>
	if (lc $p->current_element eq 'url')
	{
		if($p->within_element('artist'))
		{
			$count++;
			$urlnum++;
			$output .= "\t\t\t\t[<a href=\"$data\">$urlnum</a>]\n"
		} elsif ($p->within_element('job'))
		{
			$output .= "<a href=\"$data\">More...</a>\n";
		} else { $link_url .= $data }

# <TITLE></TITLE>
	} elsif (lc $p->current_element eq 'title')
	{
		$link_title .= $data

# <DESC></DESC>
	} elsif (lc $p->current_element eq 'desc')
	{
		$output .= "$data";

# <RAW></RAW>
	} elsif (lc $p->current_element eq 'raw')
	{
		$output .= $data

# HTML tags and others
	} else
	{
		$output .= "$data";
	}
}
##########################################################################################
sub handle_end
{
	my $p = shift;
	my $el = shift;
	my $attribs = @_;

# </WEBPAGE>
	if( $el =~ /\bwebpage\b/i)
	{
		$output .= "<div class=\"footer\">\n";
		if($showhitcount==1)
		{
#				my %stats;
#				my $file;
#				my $count;
#				# read hitcount from count.dat
#				open(HITCOUNT,"count.dat");
#				while(<HITCOUNT>)
#				{
#					chop;
#					($file,$count) = split(/=/,$_);
#					$stats{$file} = $count;
#				}
#				my $hitcount = $stats{$xmlfile};
#				close(HITCOUNT);
#				$output .= "$hitcount hits at $curdate.\n";
		}
		if($showlinkcount==1)
		{
			$output .= "Currently, you have a choice of <em>$count</em> links from this page.  Enjoy.<br>\n";
		}
		$output .= "<cite>$title</cite> &copy; $author 1999. Updated: $moddate\n";
#		$output .= "</div>\n";

#		$output .= "</body></html>\n";

# </SUBSECTION>
	} elsif( $el =~ /\bsubsection\b/i)
	{
		if($#links >= 0)
		{
			$output .= "\t<div class=\"subsection\">\n";

			$link_url = $links[0][0];
			$link_title = $links[0][1];
			$output .= "\t\t<a href=\"$link_url\">$link_title</a>\n";

			my $i;
			for $i (1 .. $#links)
			{
				$link_url = $links[$i][0];
				$link_title = $links[$i][1];
				$output .= "\t\t~ <a href=\"$link_url\">$link_title</a>\n";
			}
			$output .= "\t</div>\n\n";
		}
# </SECTION>
	} elsif( $el =~ /\bsection\b/i)
	{
		$output .= "</div>\n\n";

# </LINK>
	} elsif( $el =~ /\blink\b/i)
	{
		$count++;
		@links = (@links, [$link_url, $link_title]);
		if($ishit)
		{
			@hitpicks = (@hitpicks, [$link_url, $link_title]);
		}
		if($isnew)
		{
			@whatsnew = (@whatsnew, [$link_url, $link_title]);
		}

		foreach my $kw (@referkeywords)
		{
			if( $link_title =~ /$kw/i )
			{
				$issearchresult = 1;
			}
		}

		if($issearchresult)
		{
			@searchresults = (@searchresults, [$link_url, $link_title]);
		}

		$link_url = "";
		$link_title = "";
		$ishit = 0;
		$isnew = 0;
		$issearchresult = 0;

# </INTRO>
	} elsif( $el =~ /\bintro\b/i)
	{
		$output .= "\t</div>\n";	# intro
		$output .= "</div>\n";	# section

# <SEARCHRESULTS/>
	} elsif ($el =~ /\bsearchresults\b/i)
	{
		if($referengine ne "Nosearch")
		{
			$output .= "\n\t\t<h3>$referengine Search Results:</h3>\n";
			$output .= "\t\t<p class=\"desc\">It appears you found this page from <a href=\"$ENV{HTTP_REFERER}\"><b>$referengine</b></a>.  The keywords &quot;<i>";
			$output .= join(", ", @referkeywords);
			$output .= "</i>&quot; are probably on this page somewhere so have a look.  The following links may be what you are looking for:</p>\n";
			$output .= "\t\t<div class=\"subsection\">\n";
			$output .= "XX-RESULTS-XX";
			$output .= "\t\t</div>\n";	# subsection

			$showsearchresults=1;
		}

# <FAVOURITES/>
	} elsif ($el =~ /\bfavourites\b/i)
	{
		$output .= "\n\t\t<h3>Currently Enjoying:</h3>\n";
		$output .= "\t\t<div class=\"subsection\">\n";
		$output .= "XX-HITS-XX";
		$output .= "\t\t</div>\n";	# subsection

		$showfavourites=1;

# <RECENT/>
	} elsif ($el =~ /\brecent\b/i)
	{
		$output .= "\t\t<h3>Added in the last week:</h3>\n";
		$output .= "\t\t<div class=\"subsection\">\n";
		$output .= "XX-WHATSNEW-XX";
		$output .= "\t\t</div>\n";

		$showrecent=1;

# <QUICKSEARCH/>
	} elsif ($el =~ /\bquicksearch\b/i)
	{
		$output .= "\t\t<h3>Quick Search</h3>\n";
		$output .= "\t\t\t<div class=\"subsection\">\n";
		$output .= "\t\t\t\t<!-- Search Form's courtesy of Fravia's Sealight page -->\n";
		$output .= "\t\t\t\t<FORM method=\"GET\" action=\"http://altavista.digital.com/cgi-bin/query\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"hidden\" NAME=\"pg\" VALUE=\"q\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"hidden\" NAME=\"what\" VALUE=\"web\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"hidden\" NAME=\"fmt\"  VALUE=\"\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"text\"   NAME=\"q\" size=\"30\" maxlength=\"200\" VALUE=\"\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"submit\" VALUE= \"Altavista!\">\n";
		$output .= "\t\t\t\t</FORM>\n";
		$output .= "\t\t\t\t<FORM METHOD=\"GET\" ACTION=\"http://search.yahoo.com/bin/search\">\n";
		$output .= "\t\t\t\t\t<INPUT SIZE=\"30\" maxlength=\"200\" NAME=\"p\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"submit\" VALUE=\"Yahoo!\">\n";
		$output .= "\t\t\t\t</FORM>\n";
		$output .= "\t\t\t\t<FORM METHOD=\"GET\" ACTION=\"http://www.google.com/search\">\n";
		$output .= "\t\t\t\t\t<INPUT SIZE=\"30\" maxlength=\"200\" NAME=\"q\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"submit\" name=\"sa\" VALUE=\"Google Search\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"submit\" name=\"sa\" VALUE=\"I'm feeling lucky\">\n";
		$output .= "\t\t\t\t</FORM>\n";
		$output .= "\t\t\t\t<FORM METHOD=\"GET\" ACTION=\"http://yoyo.cc.monash.edu.au/~madmick/\">\n";
		$output .= "\t\t\t\t\t<INPUT SIZE=\"30\" maxlength=\"200\" NAME=\"tomssearch\">\n";
		$output .= "\t\t\t\t\t<INPUT TYPE=\"submit\" VALUE=\"Search this page\">\n";
		$output .= "\t\t\t\t</FORM>\n";
		$output .= "\t\t\t</div>\n";

# </CDS>
	} elsif ($el =~ /\bcds\b/i)
	{
		$output .= "\t</div>\n";

# </ARTIST>
	} elsif( $el =~ /\bartist\b/i)
	{
		$output .= "\t\t\t</font>\n";

# </DISC>
	} elsif( $el =~ /\bdisc\b/i)
	{

# </DESC>
	} elsif( $el =~ /\bdesc\b/i)
	{
		$output .= "</p>\n";

# </JOB>
	} elsif( $el =~ /\bjob\b/i)
	{
		$output .= "\t</div>\n\n";

# tags we don't want to print
# TITLE, URL, NAME, RAW
	} elsif ($el =~ /\burl\b/i)
	{
	} elsif ($el =~ /\btitle\b/i)
	{
	} elsif ($el =~ /\bname\b/i)
	{
	} elsif ($el =~ /\braw\b/i)
	{

# HTML tags
	} else
	{
		$output .= "</$el>";
	}
}
##########################################################################################
sub handle_comments
{
	my $xp = shift;
	my $comment = shift;

	$output .= "<!-- $comment -->";
}
##########################################################################################
sub open_div
{
	my $class = shift;
	my $divtag = "<div class=\"$class\">";

	return( $divtag );
}

sub close_div
{
	my $class = shift;
	my $divtag = "</div>";
	return($divtag);
}

##########################################################################################
sub keywords	# extract search keywords from referer url
{
	my $referengine = shift;	# search engine

	if($referengine eq "Nosearch")
	{
		return ();	# normal page generation
	}

	# get url from first parameter
	my $url = lc $ENV{HTTP_REFERER};

	# split query parameters off the end
	my ($site,$query) = split( /\?/, $url);

	# the parameter we are interested in varies depending on the site
	my $value = '';

	if($referengine eq "TomsSearch")
	{
		# fire up the CGI module
		my $cgi = new CGI($ENV{QUERY_STRING});

		# get the value of the parameter
		$value = $cgi->param('tomssearch');

		# remove any quotes or + signs
		$value =~ s/"//g; $value =~ s/\+//g;
	} elsif( $query gt " ")
	{
		# fire up the CGI module
		my $cgi = new CGI($query);

		# get the value of the parameter
		if( $referengine eq "Yahoo" ) { $value = $cgi->param('p'); }
		elsif( $referengine eq "Excite" ) { $value = $cgi->param('search'); }
		elsif( $referengine eq "Ultraseek" ) { $value = $cgi->param('qt'); }
		elsif( $referengine eq "Altavista" ) { $value = $cgi->param('q'); }
		elsif( $referengine eq "Anzwers" ) { $value = $cgi->param('query'); }
		elsif( $referengine eq "Google" ) { $value = $cgi->param('q'); }
		elsif( $referengine eq "FireBall" ) { $value = $cgi->param('q'); }
		elsif( $referengine eq "HotBot" ) { $value = $cgi->param('MT'); }
		elsif( $referengine eq "CNet" ) { $value = $cgi->param('query'); }
		elsif( $referengine eq "Iltrovatore" ) { $value = $cgi->param('what'); }
		elsif( $referengine eq "Go2Net" ) { $value = $cgi->param('general'); }
		elsif( $referengine eq "NineMSN" ) { $value = $cgi->param('MT'); }
		elsif( $referengine eq "MSN" ) { $value = $cgi->param('MT'); }

		# remove any quotes or + signs
		$value =~ s/"//g; $value =~ s/\+//g;
	}

	# split into single words
	my @kws = split(' ',$value);
	my @kws2 = ();

	foreach my $kw (@kws)
	{
		if($kw ne 'the' && $kw ne 'an' && $kw ne 'it'
			&& $kw ne 'of' && $kw ne 'is' && $kw ne 'and'
			&& $kw ne 'or')
		{
			@kws2 = (@kws2, $kw);
		}
	}
	return @kws2;
}
