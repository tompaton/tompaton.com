#ifndef HTML_H
#define HTML_H

#include <iostream.h>
#include <string>

class Tag {
public:
	string cgi_content() { return "Content-type: text/html\n\n"; };

	string title(string s) { return "<head><title>"+s+"</title></head>\n"+"<body>\n"; };
	string h1(string s) { return "<h1>"+s+"</h1>"; };
	string h2(string s) { return "<h2>"+s+"</h2>"; };
	string p(string s) { return "<p>"+s+"</p>"; };
	string footer() { return "</body>"; };

	string heading(string s) { return h1(s); };
	string subheading(string s) { return h2(s); };
	string url(string text, string addr) { return "<a href=\""+addr+"\">"+text+"</a>"; }
};

class HTML {
	private:
		ostream &outstream;
		Tag tags;
	public:
		HTML(ostream &out):outstream(out) {};

		write(string s) { outstream << s; };
		writeln(string s) { outstream << s << endl; };
	
		cgi_content() { write(tags.cgi_content()); };

		title(string s) { 	write(tags.title(s)); };
		h1(string s) { 		writeln(tags.h1(s)); };
		h2(string s) { 		writeln(tags.h2(s)); };
		p(string s) { 		writeln(tags.p(s)); };
		footer() { 		writeln(tags.footer()); };
		close() { 		writeln(tags.footer()); };

		heading(string s) { h1(s); };
		subheading(string s) { h2(s); };
};

#endif HTML_H

