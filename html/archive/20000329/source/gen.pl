#!/usr/local/bin/perl -I.
##########################################################################################
# perl -w gen.pl
# >> Generates HTML files from the given XML file.
# >> This is performed in two phases, 1: XML content --> XML structure
#									 and 2: XML structure --> HTML format
# >> Generates a HTML file for each <section> in the <webpage>
use strict;

use XML::Parser;

# Process command line arguments
my %options = ();
use Getopt::Std;
getopts("i:t:o:12",\%options);

my ($phase1,$phase2) = (1,1);
if (defined $options{1} and not defined $options{2})
{
	($phase1,$phase2) = (1,"");
} 
if (not defined $options{1} and defined $options{2})
{
	($phase1,$phase2) = ("",1);
}

my $xmlfile = defined $options{i} ? $options{i} : "index.xml";
my $tempfile = defined $options{t} ? $options{t} : "temp.xml";
my $template = defined $options{o} ? $options{o} : "template.xml";

# global formatting variables
$::indent = 0;
$::tabs = "";

# get modified date and todays date
my @months = qw(January February March April May June July August September October November December);
my @days = qw(Sunday Monday Tuesday Wednesday Thursday Friday Saturday);
my @daysinmonth = qw(31 28 31 30 31 30 31 31 30 31 30 31);

#	my $mtime = (stat("$xmlfile"))[9];
#	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($mtime);
#	$year += 1900;
#	my $moddate ="$days[$wday], $mday $months[$mon] $year";

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
my $curdate = "$days[$wday], $mday $months[$mon] $year";

$mday -= 7;
if($mday <1) { $mday += $daysinmonth[$mon]; $mon --; }
if($mon < 0) { $mon = 11; $year --; }

my $olddate = sprintf("%4d%02d%02d",$year,$mon+1,$mday);

##########################################################################################

print STDERR <<BANNER;
===============================================================================
 Tom's Homepage Generator
-------------------------------------------------------------------------------
        Input File: $xmlfile
 Intermediate File: $tempfile
   Generation Date: $curdate
   
BANNER


##########################################################################################
if($phase1)
{
print STDERR <<PHASE1;
-------------------------------------------------------------------------------
 Phase 1   (generating temporary XML file)

PHASE1

print STDERR "   Loading \"$xmlfile\"...";

use Phase1;
die "  Can't find input file \"$xmlfile\"" unless -f $xmlfile;

my $parser = new XML::Parser(Style => 'objects', Pkg => 'Phase1', ErrorContext => 2);

my @parsetree = $parser->parsefile($xmlfile);

print STDERR " Done.\n";

my $site = $parsetree[0][0];

print STDERR "   Validating \"$xmlfile\"...";
if($site->rvalidate(\&error_handler))
{
	print STDERR " Done.\n";
	
	print STDERR "   Outputting \"$tempfile\"...";

	my $linkcount = 0;
	
	{
		open(OUTFILE,">$tempfile");
		local(*STDOUT) = *OUTFILE;

		$linkcount = $site->output($curdate, $olddate);
		
		close(OUTFILE);
	}
	
	print STDERR " Done.\n";
	
	print STDERR "   $linkcount Links.\n";
}
}
##########################################################################################
if($phase2)
{
print STDERR <<PHASE2;
-------------------------------------------------------------------------------
 Phase 2   (generating HTML from temporary XML file)
 
PHASE2

use Phase2;
print STDERR "   Loading output template \"$template\"...";

process_template($template,"Phase2");	# in Phase2.pm

print STDERR " Done.\n\n";

print STDERR "   Loading \"$tempfile\"...";

die "  Can't find input file \"$tempfile\"" unless -f $tempfile;

my $parser = new XML::Parser(Style => 'objects', Pkg => 'Phase2', ErrorContext => 2);

my @parsetree = $parser->parsefile($tempfile);

print STDERR " Done.\n";

my $site = $parsetree[0][0];

print STDERR "   Validating \"$tempfile\"...";
if($site->rvalidate(\&error_handler))
{
	print STDERR " Done.\n";
	print STDERR "   Outputting HTML files...\n";
 	$site->output("output/");
	print STDERR " Done.\n";
}
}
print STDERR <<DONE;

 Generation Complete. 
===============================================================================
DONE


##########################################################################################
sub error_handler
{
	my $errstr = shift;
	print "ERROR: $errstr\n";
}

