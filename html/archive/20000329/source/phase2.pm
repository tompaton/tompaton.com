################################################################################
# Phase2.pm
# These modules are used to parse the temp.xml file and output html files.
################################################################################

# this sub takes a filename and reads in the "template" structure and evals the 
# required tag classes into this package...
sub process_template
{
	my ($templatefile,$pkg) = @_;
	
	die "\n     Can't find input file \"$templatefile\"\n" unless -f $templatefile;

	open(TEMPLATEFILE,$templatefile);

	my $code = "";
	LINE: while(<TEMPLATEFILE>)
	{
		# process line by line, assume it is well formed, with one tag per line.
		next LINE if (m|^\s*$|);
		
		if(m|<tag\s+name="(\w+)"(?:\s+(validate="no")\s*)?>|)
		{
			$code .= "\npackage $pkg\::$1;\n";
			$code .= "\@ISA = qw( Phase2::Element );\n";
			$code .= "sub rvalidate {1}\n" if(defined $2);
		}
 		elsif (m|<output>(.+)</output>|)
 		{
 			$code .= "\$template = '$1';\n";
 		}
		elsif(m|<output>|)
		{
			my $remainder = $';
				# allow the use of single quotes in <output></output>
			$remainder =~ s/'/\\'/g;	# put a backslash in front...
			 
			$code .= "\$template = '$remainder";	

			OUTPUT: while(<TEMPLATEFILE>)
			{
				if( m|</output>|)
				{
					last OUTPUT;
				}
				else
				{
						# allow the use of single quotes in <output></output>
					s/'/\\'/g;	# put a backslash in front...
					$code .= $_;
				}
			}
			
			$remainder = $`;
				# allow the use of single quotes in <output></output>
			$remainder =~ s/'/\\'/g;	# put a backslash in front...
			$code .= "$remainder';\n";
		}
		elsif (m|<(\w+)>([\s\w]+)</\1>|)	# will match <rattrs>blah lbha lbha</rattrs> 
		{
			$code .= "\@$1 = qw($2);\n";	# print out as @rattrs = qw(blah lbha lbha);
		}
	
	}

	close(TEMPLATEFILE);

	eval $code;
	die "Something went wrong: $@" if ($@);

}

################################################################################
package Phase2::Element;
use HTML::Entities;
@ISA = qw( XML::ValidatingElement );
$template = "";
sub output
{
	my ($self) = @_;
    my $type = ref $self;

	my @lines = split(/@@/,${"$type\::template"});
	my ($text,$command) = ("","");
	while(@lines)
	{
		($text,$command,@lines) = @lines;
			
		if(defined $text)
		{
			$text =~ s/\\T/$::tabs/g;
			print $text;
		}
			
		if(defined $command)
		{
			my $found="";
			# check if command is generic CONTENT tag, which signifies output all children
			if( lc $command eq "content" )
			{
				if(defined $self->{Kids})
				{
				    my @kids = @{ $self->{Kids} };
				    if ($#kids >= 0)
				    {
						$::tabs = "\t" x (++$::indent);

				        foreach (@kids)
						{
				            # Allow for outputting of char data unless overridden
				            if ((ref $_) =~ /::Characters$/o)
				            {
								$char = $_->{Text};
								for ($char)		# remove preceeding and trailing white space
								{
									s/^\s+//;
									s/\s+$//;
								}
								print encode_entities( $char );
				            }
				            else
				            {
								$_->output();
				            }
						}

						$::tabs = "\t" x (--$::indent);
					}
				}
				
				$found = 1;
			}
			elsif( lc $command eq "rattrs" )
			{
				foreach my $attr ( @{"$type\::rattrs"} )
				{
					print " $attr=\"$self->{$attr}\"" if(defined $self->{$attr});
				}
				$found = 1;
			}
			elsif( lc $command eq "oattrs" )
			{
				foreach my $attr ( @{"$type\::oattrs"} )
				{
					print " $attr=\"$self->{$attr}\"" if(defined $self->{$attr});
				}
				$found = 1;
			}

			# check to see if command matches an attribute
			if(not $found)
			{
				foreach my $attr ( (@{"$type\::oattrs"},@{"$type\::rattrs"}))
				{
					if( lc $command eq lc $attr )
					{
						# if so, and it is defined, print it's value
						print $self->{$attr} if(defined $self->{$attr});
						$found=1;
						last;
					}
				}
			}
			# check to see if the command matches a child tag
			if(not $found)
			{
				foreach my $kid ((@{"$type\::okids"},@{"$type\::rkids"}))
				{
					if( lc $command eq lc $kid)
					{
						# if so, then print their contents...
						$self->print_kid($kid);
						$found=1;
						last;
					}
				}
				
				# if it wasn't a subroutine name, a child tag or an attribute, then we'll print an "error"
				print "\n<!-- Unknown Template Command $command -->\n" unless $found;
			}
		}
	}
}
sub print_kid
{
	my ($self, $kid) = @_;
	
    my @kids = @{ $self->{Kids} };

    if ($#kids >= 0)
    {
		$::tabs = "\t" x (++$::indent);
	
		foreach (@kids)
		{
			$_->output() if((ref $_) =~ /::$kid$/)
		}
		
		$::tabs = "\t" x (--$::indent);
	}

}

################################################################################
package Phase2::site;
@ISA = qw( Phase2::Element );
@rattrs = qw( title author description );
@okids  = qw( page );

sub output
{
	my ($self,$outdir) = @_;
    my @kids = @{ $self->{Kids} };

    if ($#kids >= 0)
    {
		foreach (@kids)
		{
			if((ref $_) =~ /::page$/o)
			{
				print STDERR "    $_->{file}...";
				open(HTML,">$outdir$_->{file}");
				local(*STDOUT) = *HTML;
				
				$_->output();
				
				close(HTML);
				print STDERR " Done.\n";
			}
		}
	}
}


################################################################################
1;	# so use/require succeeds.
