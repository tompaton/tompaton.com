################################################################################
# Phase1.pm
# These modules are used to parse the index.xml file and output a temp.xml file.

use XML::ValidatingElement;

################################################################################
package Phase1::Element;
use HTML::Entities;
@ISA = qw( XML::ValidatingElement );
@oattrs = qw( );
@rattrs = qw( );
@okids  = qw( );
@rkids  = qw( );
$pre = '';
$post = '';
$seperator = '';
@pad = (' ','','',' ');
sub pre
{
	my $self = shift;
    my $type = ref $self;
	return ${"$type\::pre"} if(defined ${"$type\::pre"});
	
	my $tag = $type;
	$tag =~ s/(\w+::)(\w+)/$2/;
	
	my $pre = "";
	foreach my $attrib (@{"$type\::rattrs"},@{"$type\::oattrs"})
	{
		if(defined $self->{$attrib} and $self->{$attrib} gt "")
		{
			$pre .= " $attrib=\"$self->{$attrib}\"";
		}
	}
	my ($pad0,$pad1) = (defined @{"$type\::pad"}) ? (${"$type\::pad"}[0],${"$type\::pad"}[1]) : ($pad[0],$pad[1]);
	
	my $ret = "$pad0<$tag$pre>$pad1";
	$ret =~ s/\\T/$::tabs/g;
	return $ret;
}
sub post
{
	my $self = shift;
    my $type = ref $self;
	return ${"$type\::post"} if(defined ${"$type\::post"});

	my $tag = $type;
	$tag =~ s/(\w+::)(\w+)/$2/;

	my ($pad2,$pad3) = (defined @{"$type\::pad"}) ? (${"$type\::pad"}[2],${"$type\::pad"}[3]) : ($pad[2],$pad[3]);
	
	my $ret = "$pad2</$tag>$pad3";
	$ret =~ s/\\T/$::tabs/g;
	return $ret;
}
sub output
{
	my $self = shift;
    my $type = ref $self;

	print $self->pre();

	$::tabs = "\t" x (++$::indent);
	
	if(defined $self->{Kids})
	{
	    my @kids = @{ $self->{Kids} };
	    if ($#kids >= 0)
	    {
			my $first = 1;
	        foreach (@kids)
			{
	            # Allow for outputting of char data unless overridden
	            if ((ref $_) =~ /::Characters$/o)
	            {
					$char = $_->{Text};
					for ($char)		# remove preceeding and trailing white space
					{
						s/^\s+//;
						s/\s+$//;
					}
					print encode_entities( $char );
	            }
	            else
	            {
					print $seperator if($first==0);
					$first = 0;
	                
					$_->output();
	            }
			}
		}
	}

	$::tabs = "\t" x (--$::indent);
	
	print $self->post();
}
################################################################################
package Phase1::site;
@ISA = qw( Phase1::Element );
@rattrs = qw( title author description stylesheet template );
@okids  = qw( page );

sub output
{
	my ($self, $curdate, $olddate)=@_;
    my @kids = @{ $self->{Kids} };

	my $linkcount = 0;
	
    if ($#kids >= 0)
    {
		print "<?xml version=\"1.0\"?>\n<site title=\"$self->{title}\" author=\"$self->{author}\" description=\"$self->{description}\">\n";

		$::tabs = "\t" x (++$::indent);

 		my @pages = ();
        foreach (@kids)
		{
            push(@pages, $_) if ((ref $_) =~ /::page$/o);
		}

		if( $#pages > 0 )
		{		
			# collect pages for page menu
			$self->{pagemenu} = "\\T<pagemenu>\n";
	        foreach (sort {$a->{title} cmp $b->{title} } @pages)
			{
				if( lc $_->{nomenu} ne "yes")
				{
					$self->{pagemenu} .= "\\T\t<pitem url=\"$_->{menuid}.html\" title=\"$_->{title}\"/>\n";
				}
			}
			$self->{pagemenu} .= "\\T</pagemenu>\n";
		
			# output pages
	        foreach (@pages)
			{
                $linkcount += $_->output($self, $curdate, $olddate, $self->{title}, $self->{author}, $self->{stylesheet}, $self->{template});
			}
		}
		$::tabs = "\t" x (--$::indent);
		
		print "</site>\n";
	}
	return $linkcount;
}

sub print_index
{
	my $self = shift;
	my $linkcount = 0;
	
	print "\n<!-- print_index -->\n";
	
	return $linkcount;
}

sub print_list
{
	my ($self,$func) = @_;
	my $linkcount = 0;

    my @kids = @{ $self->{Kids} };
	foreach (@kids)
	{
		$linkcount += eval("\$_->$func()") if ((ref $_) =~ /::page$/o);
	}
	return $linkcount;
}

sub print_broken
{
	my $self = shift;
	return $self->print_list("print_broken");
}
sub print_new
{
	my $self = shift;
	return $self->print_list("print_new");
}
sub print_favourites
{
	my $self = shift;
	return $self->print_list("print_favourites");
}
################################################################################
package Phase1::page;
@ISA = qw( Phase1::Element );
@rattrs = qw( title menuid );
@oattrs = qw( area stylesheet template nomenu );
@okids  = qw( desc section include raw );

sub output
{
	my ($self, $site, $curdate, $olddate, $sitetitle, $author, $stylesheet, $template)=@_;
	my $linkcount = 0;

	my @kids = @{ $self->{Kids} };
	if ($#kids >= 0)
	{

		# allow overriding of defaults:
		if(defined $self->{template} and $self->{template} gt "")
		{
			# templates replace
			$template= $self->{template};
		}
			
		print "$::tabs<page file=\"$self->{menuid}.html\" windowtitle=\"$sitetitle - $self->{title}\" title=\"$self->{title}\" template=\"$template\">\n"
			. "$::tabs\t<author name=\"$author\"/>\n";
			
		$stylesheet =~ /(\w+)\.sty/;
		print "$::tabs\t<stylesheet name=\"$1\" href=\"$stylesheet\"/>\n";
		if(defined $self->{stylesheet} and $self->{stylesheet} gt "")
		{
			$self->{stylesheet} =~ /(\w+)\.sty/;
			print "$::tabs\t<stylesheet name=\"$1\" href=\"$self->{stylesheet}\"/>\n";
		}
		

		$::tabs = "\t" x (++$::indent);



		# print page menu
		my $temp = $site->{pagemenu};
		$temp =~ s/\\T/$::tabs/g;
		print $temp;
		
		# print section menu
		my @sections = ();
		my $menuid=1;
		foreach (@kids)
		{
			if ((ref $_) =~ /::section$/o)
			{
				push(@sections, $_);
				if(not defined $_->{menuid})
				{
					$_->{menuid} = $menuid++;
				}
			}
		}
		if ($#sections > 0)
		{
			@sections = sort {$a->{title} cmp $b->{title}} @sections;
			
			print "$::tabs<sectionmenu>\n";
			foreach (@sections)
			{
				if( not defined $_->{nomenu} or lc $_->{nomenu} ne "yes")
				{
					print "$::tabs\t<sitem url=\"#$_->{menuid}\" title=\"$_->{title}\"/>\n";
				}
			}
			print "$::tabs</sectionmenu>\n";
		}
		
		
		foreach (@kids)
		{
			if ((ref $_) =~ /::desc$/o)
			{
				$_->output();
			}
			# output sections data
			if ((ref $_) =~ /::section$/o)
			{
				$linkcount += $_->output($self, $olddate);
			}
			if ((ref $_) =~ /::raw$/o)
			{
				$_->output();
			}
			if ((ref $_) =~ /::include$/o)
			{
				$linkcount += $_->output($site, $olddate);
			}
		}
		
		print "$::tabs<footer date=\"$curdate\"><linkcount>$linkcount</linkcount></footer>\n";

		$::tabs = "\t" x (--$::indent);

		print "$::tabs</page>\n";
	}
	
	return $linkcount;
}

sub print_favourites
{
	my $self = shift;
	return $self->print_list("print_favourites");
}
sub print_new
{
	my $self = shift;
	return $self->print_list("print_new");
}
sub print_broken
{
	my $self = shift;
	return $self->print_list("print_broken");
}
sub print_list
{
	my ($self,$func) = @_;
	my $linkcount = 0;

	my @kids = @{ $self->{Kids} };
	if ($#kids >= 0 and $self->{"should_$func"})
	{
		print qq|$::tabs<linktopage url="$self->{menuid}.html" title="$self->{title}">\n|;	
		$::tabs = "\t" x (++$::indent);
			
		foreach (@kids)
		{
			if ((ref $_) =~ /::section$/o)
			{
				$linkcount += eval("\$_->$func(\"$self->{menuid}.html\")");
			}
		}			

		$::tabs = "\t" x (--$::indent);
		print qq|$::tabs</linktopage>\n|;
	}
	return $linkcount;
}

################################################################################
package Phase1::section;
@ISA = qw( Phase1::Element );
@rattrs = qw( title );
@oattrs = qw( nomenu menuid );
@okids = qw( desc link form );

sub output
{
	my ($self, $page, $olddate) = @_;

	my $linkcount = 0;
	
	# define some new properties
	$self->{favourite_list} = [];
	$self->{new_list} = [];
	$self->{broken_list} = [];
	
	
	print "$::tabs<section anchor=\"$self->{menuid}\" title=\"$self->{title}\">\n";

	$::tabs = "\t" x (++$::indent);

	# pull out desc and forms first, put links into two lists, one with descs, one without
	my @withdesc = ();
	my @without = ();
	
	my @kids = @{ $self->{Kids} };
	if ($#kids >= 0)
	{
		foreach (@kids)
		{
			$_->output() if ((ref $_) =~ /::desc$/o);
			$_->output() if ((ref $_) =~ /::form$/o);
			
			if ((ref $_) =~ /::link$/o)
			{
				# add to favourites
				if(defined $_->{hit} and lc $_->{hit} eq "yes")
				{
					push(@{$self->{favourite_list}},$_);
					$page->{should_print_favourites} = 1;
				}
				# add to new
				if(defined $_->{date} and $_->{date} gt $olddate)
				{
					push(@{$self->{new_list}},$_);
					$page->{should_print_new} = 1;
				}
				# add to broken
				if(defined $_->{broken} and lc $_->{broken} eq "yes")
				{
					push(@{$self->{broken_list}},$_);
					$page->{should_print_broken} = 1;
				}
				
				if($_->has_desc())
				{
					push(@withdesc,$_);
				} else
				{
					push(@without,$_);
				}
			}
		}
		
		# now output links without descriptions
		if($#without >= 0)
		{
			if($#withdesc >= 0)
			{
				print "$::tabs<linkbox>\n";
				$::tabs = "\t" x (++$::indent);
				foreach (@without)
				{
					$_->output($olddate,"linkb");
					$linkcount += $_->url_count();
				}
				$::tabs = "\t" x (--$::indent);
				print "$::tabs</linkbox>\n";
			} else
			{
				print "$::tabs<linknodesc>\n";
				$::tabs = "\t" x (++$::indent);
				foreach (@without)
				{
					$_->output($olddate,"link");
					$linkcount += $_->url_count();
				}
				$::tabs = "\t" x (--$::indent);
				print "$::tabs</linknodesc>\n";
			}
		}
		# now output links with descriptions
		if($#withdesc >= 0)
		{
			print "$::tabs<linkdesc>\n";
			$::tabs = "\t" x (++$::indent);
			foreach (@withdesc)
			{
				$_->output($olddate,"linkd");
				$linkcount += $_->url_count();
			}
			$::tabs = "\t" x (--$::indent);
			print "$::tabs</linkdesc>\n";
		}
	}
	
	$::tabs = "\t" x (--$::indent);

	print "$::tabs</section>\n";

	return $linkcount;
}

sub print_favourites
{
	my ($self,$url) = @_;
	return $self->print_list($self->{favourite_list},$url);
}
sub print_new
{
	my ($self,$url) = @_;
	return $self->print_list($self->{new_list},$url);
}
sub print_broken
{
	my ($self,$url) = @_;
	return $self->print_list($self->{broken_list},$url);
}
sub print_list
{
	my ($self,$rlist,$url) = @_;
	my $linkcount = 0;
	if(@{$rlist})
	{
		print qq|$::tabs<linktosection url="$url#$self->{menuid}" title="$self->{title}">\n|;	
		$::tabs = "\t" x (++$::indent);
			
		foreach (@{$rlist})
		{
			print "$::tabs<linkdesc>\n";
			$_->output();
			$linkcount += $_->url_count();
			print "$::tabs</linkdesc>\n";
		}	
		$::tabs = "\t" x (--$::indent);
		print qq|$::tabs</linktosection>\n|;
	}
	return $linkcount;
}
################################################################################
package Phase1::link;
@ISA = qw( Phase1::Element );
@oattrs = qw( date hit broken );
@okids = qw( url desc title );

sub output
{
	my ($self,$olddate,$tag) = @_;

	$tag = defined $tag ? $tag : "linkd";

	print "$::tabs<$tag>\n";
	
	$::tabs = "\t" x (++$::indent);

	# print url
	my $hastitleurl = "";
	foreach (@{$self->{Kids}})
	{
		if (((ref $_) =~ /::url$/o) and (not defined $_->{linktext}))
		{
			$hastitleurl = 1;
			print "$::tabs<urltitle url=\"";
			$_->output();
			print "\">";
			foreach (@{$self->{Kids}})
			{
				$_->output(),last if ((ref $_) =~ /::title$/o);
			}			
			print "</urltitle>\n";
			last;
		}
	}
	# print flags (but only if outdate defined, otherwise we are in a favourite/new list anyway...)
	if(defined $olddate)
	{
		print "$::tabs<hit/>\n" if(defined $self->{hit} and lc $self->{hit} eq "yes");
		print "$::tabs<new/>\n" if(defined $self->{date} and $self->{date} gt $olddate);
	}
	print "$::tabs<broken/>\n" if(defined $self->{broken} and lc $self->{broken} eq "yes");

	# print title 
	if (not $hastitleurl)
	{
		foreach (@{$self->{Kids}})
		{
			if ((ref $_) =~ /::title$/o)
			{
				print "$::tabs<title>";
				$_->output();
				print "</title>\n";
				last;
			}
		}
	}
	
	if ( $self->has_desc() )
	{
		foreach (@{$self->{Kids}})
		{
			if ((ref $_) =~ /::desc$/o)
			{
				$_->output();
				last;
			}
		}
		
		# print links
		foreach (@{$self->{Kids}})
		{
			if (((ref $_) =~ /::url$/o) and (defined $_->{linktext}))
			{
				print "$::tabs<url linktext=\"$_->{linktext}\">";
				$_->output();
				print "</url>\n";
			}
		}
	}
	  	
	$::tabs = "\t" x (--$::indent);
	
	print "$::tabs</$tag>\n";
}

sub has_desc
{
	my $self = shift;
	my @kids = @{ $self->{Kids} };
	if ($#kids >= 0)
	{
		foreach (@kids)
		{
			return 1 if ((ref $_) =~ /::desc$/o);
		}
	}	
	return '';
}
sub url_count
{
	my $self = shift;
	my @kids = @{ $self->{Kids} };
	my $linkcount = 0;
	if ($#kids >= 0)
	{
		foreach (@kids)
		{
			$linkcount++ if ((ref $_) =~ /::url$/o);
		}
	}	
	return $linkcount;
}

package Phase1::url;
@ISA = qw( Phase1::Element );
@oattrs = qw( linktext );
@pad = ('\T',"","","\n");
$pre = ''; $post = '';

package Phase1::title;
@ISA = qw( Phase1::Element );
@okids = qw( q );
@pad = ('\T',"","","\n");
$pre = ''; $post = '';

################################################################################
package Phase1::desc;
@ISA = qw( Phase1::Element );
@okids = qw( cite q img br em a );
@pad = ('\T',"","","\n");
sub get_contents
{
	
}
################################################################################
package Phase1::include;
@ISA = qw( Phase1::Element );
@rattrs = qw( type src );
@oattrs = qw( parser );

sub output
{
	my ($self, $site, $olddate) = @_;
	my $linkcount = 0;
	
	if(lc $self->{type} eq "xml")
	{
		print "\n<!-- Included XML file \"$self->{src}\" parser \"$self->{parser}\"-->\n\n";
	}
	elsif (lc $self->{type} eq "html")
	{
		print "$::tabs<include type=\"html\" src=\"$self->{src}\"/>\n";
	}
	elsif (lc $self->{type} eq "func")
	{
		$linkcount += eval("\$site->$self->{src}()");
		print "\n<!-- $@ -->\n" if($@);
	}
	
	return $linkcount;
}

################################################################################
# Simple XML elements, these will basically be validated and passed through
# to the temp file untouched (by the Phase1::Element class)

package Phase1::raw;
@ISA = qw( Phase1::Element );
@okids = qw( p );
@pad = ('\T',"\n",'\T',"\n");

package Phase1::p;
@ISA = qw( Phase1::Element );
@oattrs = qw( style class );
@okids = qw( a cite em );
@pad = ('\T',"","","\n");

package Phase1::cite;
@ISA = qw( Phase1::Element );

package Phase1::em;
@ISA = qw( Phase1::Element );

package Phase1::q;
@ISA = qw( Phase1::Element );

package Phase1::br;
@ISA = qw( Phase1::Element );
@pad = ("","","","\n\\T");

package Phase1::img;
@ISA = qw( Phase1::Element );
@rattrs = qw( src );
@oattrs = qw( width height border alt align );

package Phase1::a;
@ISA = qw( Phase1::Element );
@rattrs = qw( href );
@oattrs = qw( );
@okids = qw( img );

package Phase1::form;
@ISA = qw( Phase1::Element );
@rattrs = qw( method action );
@okids = qw( input );
@pad = ('\T',"\n",'\T',"\n");

package Phase1::input;
@ISA = qw( Phase1::Element );
@oattrs = qw( name type value maxlength size  );
@okids = qw( );
@pad = ('\T',"","","\n");

################################################################################
1;	# so use/require succeeds.
