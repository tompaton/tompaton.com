#!/usr/local/bin/perl -I~tomp/WWW/cgi-bin:~mjs/local/lib/CPAN
##########################################################################################
use XML::Parser;
use Snippets;
use strict;

my $xmlfile = shift;		# for command line source file

die "Content-type: text/html\n\nCan't find input file \"$xmlfile\"" unless -f $xmlfile;

my $parser = new XML::Parser(Style => 'objects', Pkg => 'Snippets', ErrorContext => 2);

my @parsetree = $parser->parsefile($xmlfile);

my $snippets = $parsetree[0][0];

if($snippets->rvalidate(\&error_handler))
{
	$snippets->output();
#	use Data::Dumper;
#	print Dumper($snippets);
}
##########################################################################################
sub error_handler
{
	my $errstr = shift;
	print "ERROR: $errstr\n";
}
