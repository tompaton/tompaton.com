<?xml version="1.0"?>
<webpage
	title="Tom's CSS Quick Reference"
	author="Tom Paton"
	description ="A Cascading Style Sheet quick reference"
	stylesheet="tomstyle1">

<!-- Intro -->
<intro>
	<p>This is a simple reference guide to the Cascading Style Sheet attributes.</p>
	<p>Taken largely from <cite>XML: Extensible Markup Language, Elliotte Rusty Harold, IDG Books Worldwide</cite>.</p>
	<p><a href="./index.xml">Back to my homepage.</a></p>
</intro>

<section title="Style Tags" menu="Tags" menuid="tags">
	<raw>
		<div class="subsection">
			<h3>&amp;lt;LINK rel=&quot;stylesheet&quot; type=&quot;text/css&quot; name=&quot;<em>name</em>&quot; href=&quot;<em>filename</em>&quot;&amp;gt;</h3>
				<p>Links in external style sheet (in &amp;lt;head&amp;gt;)</p>
			<h3>&amp;lt;STYLE&amp;gt;</h3>
				<p>Define styles in &amp;lt;head&amp;gt;.  Surround with &amp;lt;-- ... --&amp;gt;.</p>
			<h3>&amp;lt;SPAN&amp;gt;</h3>
				<p>Used for inline formatting.</p>
			<h3>&amp;lt;DIV&amp;gt;</h3>
				<p>Places line breaks (no vertical space) around section.</p>
			<h3>Attributes</h3>
				<h4>class=&quot;&quot;</h4>
					<p>Specifies style class.  Defined with .style</p>
				<h4>id=&quot;&quot;</h4>
					<p>Unique element id.  Defined with #id</p>
					<p>May also be used as a target &amp;lt;a href="#id"&amp;gt;</p>
				<h4>style=&quot;&quot;</h4>
					<p>Manually specify or override style for given element.</p>
		</div>
	</raw>
</section>

<section title="Lengths" menuid="lengths">
	<raw>
		<div class="subsection">
			<table border="1">
				<thead>
					<tr><td class="heading">Unit</td><td class="heading">Meaning</td></tr>
				</thead>
				<tbody>
					<tr><td class="property">em</td><td>Width of &quot;m&quot;</td></tr>
					<tr><td class="property">ex</td><td>Width of &quot;x&quot;</td></tr>
					<tr><td class="property">px</td><td>Pixels</td></tr>
					<tr><td class="property">pt</td><td>Points</td></tr>
					<tr><td class="property">in</td><td>Inches</td></tr>
					<tr><td class="property">cm</td><td>Centimeters</td></tr>
					<tr><td class="property">mm</td><td>Millimeters</td></tr>
					<tr><td class="property">pc</td><td>Picas</td></tr>
				</tbody>
			</table>
		</div>
	</raw>
</section>

<section title="Font Properties" menu="Font" menuid="font">
	<raw>
		<div class="subsection">
			<table border="1">
				<thead>
					<tr><td class="heading">Property</td><td class="heading">Values</td><td class="heading">Comments</td></tr>
				</thead>
				<tbody>
					<tr><td class="property">font-family</td>
						<td class="values"><em>name</em>, serif, sans-serif, monospace, fantasy, cursive</td>
						<td>Can specify preferences seperated by commas</td></tr>
					<tr><td class="property">font-style</td>
						<td class="values">normal, italic, oblique</td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">font-variant</td>
						<td class="values">normal, small-caps</td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">font-weight</td>
						<td class="values">normal, bold, 100, 200, 300, <b>400</b>, 500, 600, 700, 800, 900, lighter, bolder</td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">font-size</td>
						<td class="values">xx-small, x-small, small, medium, large, x-large, xx-large, larger, smaller, <em>length</em>, <em>percentage</em></td>
						<td>&amp;nbsp;</td></tr>
				</tbody>
			</table>
		</div>
	</raw>
</section>

<section title="Colour and Background Properties" menu="Background" menuid="background">
	<raw>
		<div class="subsection">
			<table border="1">
				<thead>
					<tr><td class="heading">Property</td><td class="heading">Values</td><td class="heading">Comments</td></tr>
				</thead>
				<tbody>
					<tr><td class="property">color</td>
						<td class="values"><em>colour name</em>,<em>#rrggbb</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">background-color</td>
						<td class="values"><b>transparent</b>, <em>colour name</em>, <em>#rrggbb</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">background-image</td>
						<td class="values"><b>none</b>, <em>URL</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">background-repeat</td>
						<td class="values">repeat, repeat-x, repeat-y, <b>no-repeat</b></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">background-attachment</td>
						<td class="values"><b>scroll</b>, fixed</td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">background-position</td>
						<td class="values">top, center, bottom, <em>length</em>, <em>percentage</em></td>
						<td>&amp;nbsp;</td></tr>
				</tbody>
			</table>
		</div>
	</raw>
</section>

<section title="Text Properties" menu="Text" menuid="text">
	<raw>
		<div class="subsection">
			<table border="1">
				<thead>
					<tr><td class="heading">Property</td><td class="heading">Values</td><td class="heading">Comments</td></tr>
				</thead>
				<tbody>
					<tr><td class="property">text-align</td>
						<td class="values">left, right, center, justify</td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">text-indent</td>
						<td class="values"><em>length</em>, <em>percentage</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">word-spacing</td>
						<td class="values"><b>normal</b>, <em>length</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">letter-spacing</td>
						<td class="values"><b>normal</b>, <em>length</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">text-decoration</td>
						<td class="values"><b>none</b>, underline, line-through, overline, blink</td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">vertical-align</td>
						<td class="values"><b>baseline</b>, sub, super, top, text-top, middle, bottom, text-bottom, <em>percentage</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">text-transform</td>
						<td class="values">capitalize, uppercase, lowercase, <b>none</b></td>
						<td>&amp;nbsp;</td></tr>
				</tbody>
			</table>
		</div>
	</raw>
</section>

<section title="Box Properties" menu="Box" menuid="box">
	<raw>
		<div class="subsection">
			<table border="1">
				<thead>
					<tr><td class="heading">Property</td><td class="heading">Values</td><td class="heading">Comments</td></tr>
				</thead>
				<tbody>
					<tr><td class="property">margin-top</td>
						<td class="values"><em>length</em>, <em>percentage</em>, <b>auto</b></td>
						<td rowspan="4">Space outside borders</td></tr>
					<tr><td class="property">margin-right</td>
						<td class="values"><em>length</em>, <em>percentage</em>, <b>auto</b></td>
						</tr>
					<tr><td class="property">margin-bottom</td>
						<td class="values"><em>length</em>, <em>percentage</em>, <b>auto</b></td>
						</tr>
					<tr><td class="property">margin-left</td>
						<td class="values"><em>length</em>, <em>percentage</em>, <b>auto</b></td>
						</tr>
					<tr><td class="property">margin</td>
						<td class="values"><em>length</em>, <em>percentage</em>, <b>auto</b></td>
						<td>Specify all 4 margin spaces</td></tr>
					<tr><td class="property">padding-top</td>
						<td class="values"><em>length</em>, <em>percentage</em></td>
						<td rowspan="4">Spacing inside borders</td></tr>
					<tr><td class="property">padding-right</td>
						<td class="values"><em>length</em>, <em>percentage</em></td>
						</tr>
					<tr><td class="property">padding-bottom</td>
						<td class="values"><em>length</em>, <em>percentage</em></td>
						</tr>
					<tr><td class="property">padding-left</td>
						<td class="values"><em>length</em>, <em>percentage</em></td>
						</tr>
					<tr><td class="property">padding</td>
						<td class="values"><em>length</em>, <em>percentage</em></td>
						<td>Specify all 4 internal spaces</td></tr>
					<tr><td class="property">border-width-top</td>
						<td class="values"><em>length</em>, thin, <b>medium</b>, thick</td>
						<td rowspan="4">Width of border</td></tr>
					<tr><td class="property">border-width-right</td>
						<td class="values"><em>length</em>, thin, <b>medium</b>, thick</td>
						</tr>
					<tr><td class="property">border-width-bottom</td>
						<td class="values"><em>length</em>, thin, <b>medium</b>, thick</td>
						</tr>
					<tr><td class="property">border-width-left</td>
						<td class="values"><em>length</em>, thin, <b>medium</b>, thick</td>
						</tr>
					<tr><td class="property">border-width</td>
						<td class="values"><em>length</em>, thin, <b>medium</b>, thick</td>
						<td>Specify all 4 border widths</td></tr>
					<tr><td class="property">border-color</td>
						<td class="values"><em>colour</em>, <em>#rrggbb</em></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">border-style</td>
						<td class="values"><b>none</b>, dotted, dashed, solid, double, groove, ridge, inset, outset</td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">width</td>
						<td class="values"><em>length</em>, <em>percentage</em>, <b>auto</b></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">height</td>
						<td class="values"><em>length</em>, <em>percentage</em>, <b>auto</b></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">float</td>
						<td class="values">left, right, <b>none</b></td>
						<td>&amp;nbsp;</td></tr>
					<tr><td class="property">clear</td>
						<td class="values">left, right, <b>none</b>, both</td>
						<td>&amp;nbsp;</td></tr>
				</tbody>
			</table>
		</div>
	</raw>
</section>

<section title="Classification Properties" menu="Classification" menuid="classification">
	<raw>
		<div class="subsection">
			<table border="1">
				<thead>
					<tr><td class="heading">Property</td><td class="heading">Values</td><td class="heading">Comments</td></tr>
				</thead>
				<tbody>
					<tr><td class="property">display</td>
						<td class="values"><b>block</b>, inline, list-item, none</td>
						<td>Line breaks before and after element?</td></tr>
					<tr><td class="property">white-space</td>
						<td class="values"><b>normal</b>, pre, nowraw</td>
						<td>How is whitespace treated?</td></tr>
					<tr><td class="property">list-style-type</td>
						<td class="values"><b>disc</b>, circle, square, decimal, lower-roman, upper-roman, lower-alpha, upper-alpha, none</td>
						<td>List bullet style</td></tr>
					<tr><td class="property">list-style-image</td>
						<td class="values"><em>URL</em></td>
						<td>Image for list bullet</td></tr>
					<tr><td class="property">list-style-position</td>
						<td class="values">inside, <b>outside</b></td>
						<td>Position of bullet relative to list items and wrapping</td></tr>
				</tbody>
			</table>
		</div>
	</raw>
</section>

</webpage>
