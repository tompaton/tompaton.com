#!/usr/local/bin/perl -I.
use XML::ValidatingElement;
#use strict;
##########################################################################################
package Webpage::webpage;
@ISA = qw( XML::ValidatingElement );
@okids  = qw( section intro );
@oattrs  = qw( showlinkcount stylesheet rand title author description showhitcount  );
sub output
{
	my $self = shift;
    my $type = ref $self;

# get modified date and todays date
	my @months = ("January","February","March","April","May","June","July",
	      "August","September","October","November","December");
	my @days = ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	my @daysinmonth = (31,28,31,30,31,30,31,31,30,31,30,31);

#	my $mtime = (stat("$xmlfile"))[9];
#	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($mtime);
#	$year += 1900;
#	my $moddate ="$days[$wday], $mday $months[$mon] $year";

	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	$year += 1900;
	my $curdate = "$days[$wday], $mday $months[$mon] $year";

	$mday -= 7;
	if($mday <1) { $mday += $daysinmonth[$mon]; $mon --; }
	if($mon < 0) { $mon = 11; $year --; }

	my $olddate = sprintf("%4d%02d%02d\n",$year,$mon+1,$mday);

    my @kids = @{ $self->{Kids} };
    if ($#kids >= 0)
    {
        foreach (@kids)
		{
            # Allow for outputting of char data unless overridden
            if ((ref $_) =~ /::section$/o)
            {
                $_->output($olddate);
            }
		}
	}
}
##########################################################################################
package Webpage::Element;
use HTML::Entities;
@ISA = qw( XML::ValidatingElement );
@oattrs = qw( );
@rattrs = qw( );
@okids  = qw( );
@rkids  = qw( );
$pre = "";
$post = "";
$seperator = "";
sub pre
{
	my $self = shift;
    my $type = ref $self;
	${"$type\::pre"};
}
sub post
{
	my $self = shift;
    my $type = ref $self;
	${"$type\::post"};
}

sub output
{
	my $self = shift;
	my $output = shift;
    my $type = ref $self;

#	print ${"$type\::pre"};
	print $output $self->pre();
    my @kids = @{ $self->{Kids} };
    if ($#kids >= 0)
    {
		my $first = 1;
        foreach (@kids)
		{
            # Allow for outputting of char data unless overridden
            if ((ref $_) =~ /::Characters$/o)
            {
				$char = $_->{Text};
				for ($char)		# remove preceeding and trailing white space
				{
					s/^\s+//;
					s/\s+$//;
				}
				print $output encode_entities( $char );
            }
            else
            {
				if($first==0)
				{
					print $output $seperator;
				}
				$first = 0;
                $_->output($output);
            }
		}
	}
#	print ${"$type\::post"};
	print $output $self->post();
}
##########################################################################################
package Webpage::intro;
sub rvalidate { return 1; }
sub output { };
package Webpage::cds;
sub rvalidate { return 1; }
sub output { };
##########################################################################################
package Webpage::raw;
@ISA = qw( Webpage::Element );
@okids  = qw( p a );
sub rvalidate { return 1; }
$pre = "<div class=\"desc\">\n";
$post = "</div>\n";
##########################################################################################
package Webpage::section;
@ISA = qw( Webpage::Element );
@oattrs = qw( title menu menuid );
@okids  = qw( subsection cds desc raw form );
sub file
{
	my $self = shift;
    my $type = ref $self;
	if($self->{menuid} gt " ")
	{
		return $self->{menuid};
	} else
	{
		return $self->{title};
	}
}
sub output
{
	my $self = shift;
	my $olddate = shift;
    my $type = ref $self;

	my $file = $self->file();

	open(OUTPUT,">$file.html");
	my $output = \*OUTPUT;

	print $output "<html><head><title>Tom Paton's Homepage - $self->{title}</title>";

	open(BOILERPLATE,"<child_pre.html");
	while(<BOILERPLATE>)
	{
		print $output $_;
	}
	close(BOILERPLATE);
	print $output "\n<!-- BEGIN CONTENT	-->\n";
	print $output "<h1>$self->{title}</h1>\n";

    my @kids = @{ $self->{Kids} };
    if ($#kids >= 0)
    {
        foreach (@kids)
		{
            # Allow for outputting of char data unless overridden
            if ((ref $_) =~ /::Character$/o)
            {
            } else
			{
                $_->output($output,$olddate);
			}
		}
	}

	print $output "<!-- END CONTENT -->\n";

	open(BOILERPLATE,"<child_post.html");
	while(<BOILERPLATE>)
	{
		print $output $_;
	}
	close(BOILERPLATE);

	close(OUTPUT);
}
##########################################################################################
package Webpage::subsection;
@ISA = qw( Webpage::Element );
@oattrs = qw( title );
@okids  = qw( link desc );
sub output
{
	my $self = shift;
	my $output = shift;
	my $olddate = shift;
    my $type = ref $self;
	my $first = 1;
	print $output "<h2>$self->{title}</h2>\n<div class=\"subsection\">";

    my @kids = @{ $self->{Kids} };
    if ($#kids >= 0)
    {
        foreach (@kids)
		{
            if ((ref $_) =~ /::desc$/o)
            {
                $_->output($output);
            }
            if ((ref $_) =~ /::link$/o)
            {
				if($first==0)
				{
					print $output " ~ ";
				}
				$first = 0;
                $_->output($output,$olddate);
            }
		}
	}
	print $output "</div>\n";
}
##########################################################################################
package Webpage::link;
@ISA = qw( Webpage::Element );
@oattrs = qw( date status );
@rkids  = qw( title url );
sub output
{
	my $self = shift;
	my $output = shift;
	my $olddate = shift;
    my $type = ref $self;

#	use Data::Dumper;
#	print Dumper($self);
#	die;

	my $title = "TITLE";
	my $url = "URL";
    my @kids = @{ $self->{Kids} };
    if ($#kids >= 0)
    {
        foreach (@kids)
		{
            # Allow for outputting of char data unless overridden
            if ((ref $_) =~ /::title$/o)
            {
                $title = $_->gettitle();
            } elsif ((ref $_) =~ /::url$/o)
            {
                $url = $_->geturl();
            }
		}
	}


	print $output "<a href=\"";
	print $output $url;
	print $output "\">";
	if($self->{date} gt $olddate)
	{
		if($self->{status} eq "hit")
		{
			print $output '<img src="newhit.gif" border="0">';
		} else
		{
			print $output '<img src="new.gif" border="0">';
		}
	} else
	{
		if($self->{status} eq "hit")
		{
			print $output '<img src="hit.gif" border="0">';
		}
	}
#	$self->{Kids}->{title}->output($output);
	print $output $title;
	print $output "</a>\n";
}
##########################################################################################
package Webpage::url;
@ISA = qw( Webpage::Element );
sub geturl
{
	my $self = shift;
    my $type = ref $self;
	return $self->{Kids}[0]->{Text};
}
##########################################################################################
package Webpage::title;
@ISA = qw( Webpage::Element );
sub gettitle
{
	my $self = shift;
    my $type = ref $self;
	return $self->{Kids}[0]->{Text};
}
##########################################################################################
package Webpage::q;
@ISA = qw( Webpage::Element );
@okids  = qw(  );
$pre = ' &quot;';
$post = '&quot; ';
##########################################################################################
package Webpage::desc;
@ISA = qw( Webpage::Element );
@okids  = qw( q em a );
$pre = '<p class="desc">';
$post = "</p>\n";
##########################################################################################
# HTML ELEMENTS
#------------------------------------------------------------------------------------------
package Webpage::p;
@ISA = qw( Webpage::Element );
$pre = "<p>";
$post = "</p>\n";
package Webpage::em;
@ISA = qw( Webpage::Element );
$pre = " <em>";
$post = "</em> ";
package Webpage::cite;
@ISA = qw( Webpage::Element );
$pre = " <cite>";
$post = "</cite> ";
##########################################################################################
package Webpage::a;
use HTML::Entities;
@ISA = qw( Webpage::Element );
@rattrs  = qw( href );
sub output
{
	my $self = shift;
	my $output = shift;
	my $type = ref $self;

	print $output " <a href=\"$self->{href}\">";

    my @kids = @{ $self->{Kids} };
    if ($#kids >= 0)
    {
        foreach (@kids)
		{
            # Allow for outputting of char data unless overridden
            if ((ref $_) =~ /::Characters$/o)
            {
				$char = $_->{Text};
				for ($char)		# remove preceeding and trailing white space
				{
					s/^\s+//;
					s/\s+$//;
				}
				print $output encode_entities( $char );
            } else
			{
				$_->output($output);
			}

		}
	}

	print $output "</a> ";
}
##########################################################################################
package Webpage::img;
use HTML::Entities;
@ISA = qw( Webpage::Element );
@rattrs  = qw( src );
@oattrs  = qw( width height border alt align );
sub output
{
	my $self = shift;
	my $output = shift;
	my $type = ref $self;

	print $output " <img src=\"$self->{src}\"";
	if($self->{width} gt " ") {
		print $output " width=\"$self->{width}\"";
	}
	if($self->{height} gt " ") {
		print $output " height=\"$self->{height}\"";
	}
	if($self->{border} gt " ") {
		print $output " border=\"$self->{border}\"";
	}
	if($self->{alt} gt " ") {
		print $output " alt=\"$self->{alt}\"";
	}
	if($self->{align} gt " ") {
		print $output " align=\"$self->{align}\"";
	}
	print $output "> ";
}
##########################################################################################
package Webpage::form;
use HTML::Entities;
@ISA = qw( Webpage::Element );
@rattrs  = qw( method action );
@okids = qw( input );
sub pre
{
	my $self = shift;
	my $type = ref $self;
	return "<form method=\"$self->{method}\" action=\"$self->{action}\">\n";
}
sub post
{
	return "</form>\n";
}
##########################################################################################
package Webpage::input;
use HTML::Entities;
@ISA = qw( Webpage::Element );
@oattrs  = qw( type name size maxlength value  );
sub output
{
	my $self = shift;
	my $output = shift;
	my $type = ref $self;

	print $output "<input ";
	if($self->{type} gt " ") {
		print $output " type=\"$self->{type}\"";
	}
	if($self->{name} gt " ") {
		print $output " name=\"$self->{name}\"";
	}
	if($self->{size} gt " ") {
		print $output " size=\"$self->{size}\"";
	}
	if($self->{maxlength} gt " ") {
		print $output " maxlength=\"$self->{maxlength}\"";
	}
	if($self->{value} gt " ") {
		print $output " value=\"$self->{value}\"";
	}
	print $output ">\n";
}
##########################################################################################
package Webpage::Characters;
use HTML::Entities;
sub output
{
	my $self = shift;
	my $output = shift;
    my $type = ref $self;
	$char = $self->{Text};
	for ($char)		# remove preceeding and trailing white space
	{
		s/^\s+//;
		s/\s+$//;
	}
	print $output ${ encode_entities( $char )};
}
##########################################################################################
1;  # so the require or use succeeds
