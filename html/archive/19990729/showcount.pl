#!/usr/local/bin/perl
##########################################################################################
use strict;

die "Content-type: text/plain\n\nCan't find input file \"count.dat\"" unless -f "count.dat";

##########################################################################################
my %stats;
my $count;
my $file;
my $hitcount;

open(STATS, "</www/users/madmick/cgi-bin/count.dat");
# lock file

# read current value
while(<STATS>)
{
	chop;
	($file,$count) = split(/=/,$_);
	$stats{$file} = $count;
}
close(STATS);
# unlock file

print	"Content-type: text/html\n\n";
print	"<html><head><title>Hit Statistics for Tom Paton's Web Site</title>\n";
print	"</head>\n";
print	"<body background=\"http://yoyo.cc.monash.edu.au/~madmick/bg1_0.gif\" color=\"black\" alink=\"navy\" vlink=\"navy\" alink=\"blue\">\n";
print	"<h1>Hit Statistics for Tom Paton's Web Site</h1>\n";
print	"<p>It's a <i>programming exercise</i> not an ego trip...  (really...)  And pretty interesting at that -- &quot;Pink+Bits&quot; getting to my site :)</p>";
print	"<p>Anyway, you try and see how many people actually visit your page when you keep accidently (and on the odd occassion, deliberately) nuking the counter file... (and skewing the results out of all proportion by adding funky new features and having to test them extensively to get them working).</p>\n";

print	"<table>";

my $total = $stats{"TOTAL : "};
my $lastcat = "";

foreach (sort(keys %stats))
{
	my ($cat,$item) = split(/:/,$_);
	
	if(not $item gt " ")
	{
		$item = "(none)";
	}
	if($cat ne $lastcat)
	{
		$lastcat = $cat;
		print "<tr><td colspan=3><b>$cat</b></td></tr>\n";
	}
	printf("<tr><td>&nbsp;&nbsp;&nbsp;$stats{$_}</td><td><font size=-2>%3.1f\%</font></td>",($stats{$_}/$total)*100);
	print "<td>$item</td></tr>\n";
}

print	"</table>";

print	"<p>This page is generated from a simple <a href=\"http://yoyo.cc.monash.edu.au/~madmick/cgi-bin/echo.pl?showcount.pl\">perl script</a> that reads my <a href=\"http://yoyo.cc.monash.edu.au/~madmick/cgi-bin/echo.pl?count.dat\">counter file</a>, written to by the master perl <a href=\"http://yoyo.cc.monash.edu.au/~madmick/cgi-bin/echo.pl?CGI.pl\">CGI</a> script...</p>\n";
print	"<p>To complete the perl scripts, <a
href=\"http://yoyo.cc.monash.edu.au/~madmick/cgi-bin/echo.pl?echo.pl\">echo.pl</a> displays the
file specified in it's
query string as text/plain data.</p>\n";

print	"<p><a href=\"http://yoyo.cc.monash.edu.au/~madmick/\">Main Page</a></p>\n";


print	"</body></html>\n";
