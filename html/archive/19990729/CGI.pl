#!/usr/local/bin/perl
##########################################################################################
use CGI;
use strict;
use Fcntl ':flock'; # import LOCK_* constants

##########################################################################################
# what file are we looking for?
my $xmlfile=$ENV{PATH_INFO};
$xmlfile =~ s|~madmick|www/users/madmick|g;	# translate /~madmick/ to /www/users/madmick/

die "Content-type: text/plain\n\nCan't find input file \"$xmlfile\"" unless -f $xmlfile;

##########################################################################################
# check browser type
my $realbrowser = $ENV{HTTP_USER_AGENT};
my $css = "css";	# munched browser code

# Blacklist browsers that don't display style sheets properly...
	# Mozilla/4.6 [en]C-CCK-MCD monwin/020  (Win95; I)
	# Mozilla/4.6 [en] (WinNT; I)
	# Mozilla/4.6 [en] (Win98; I)
if(($realbrowser =~ /Mozilla\/4.6/i) && ($realbrowser =~ /Win/i))
{
	$css = "nocss";
}

##########################################################################################
# check if this hit resulted from a search engine query
my $refer = $ENV{HTTP_REFERER};

# split query parameters off the end
my ($refersite,$referquery) = split( /\?/, $refer);

# the parameter we are interested in varies depending on the site
my $referengine = 'Nosearch';
my $suffix = "nosearch";

if( $referquery gt " ")
{
	if( $refersite =~ /yahoo/i ) { $referengine = 'Yahoo'; }
	elsif( $refersite =~ /excite/i ) { $referengine = 'Excite'; }
	elsif( $refersite =~ /ultraseek/i ) { $referengine = 'Ultraseek'; }
	elsif( $refersite =~ /altavista/i ) { $referengine = 'Altavista'; }
	elsif( $refersite =~ /anzwers/i ) { $referengine = 'Anzwers'; }
	elsif( $refersite =~ /google/i ) { $referengine = 'Google'; }
	elsif( $refersite =~ /hotbot/i ) { $referengine = 'HotBot'; }
	elsif( $refersite =~ /fireball/i ) { $referengine = 'FireBall'; }
	elsif( $refersite =~ /search\.cnet/i ) { $referengine = 'CNet'; }
	elsif( $refersite =~ /iltrovatore/i ) { $referengine = 'Iltrovatore'; }
	elsif( $refersite =~ /search\.go2net/i ) { $referengine = 'Go2Net'; }
	elsif( $refersite =~ /search\.ninemsn/i ) { $referengine = 'NineMSN'; }
	elsif( $refersite =~ /search\.msn/i ) { $referengine = 'MSN'; }
	$suffix = "search";
}

if( $referengine eq 'Nosearch' && $ENV{QUERY_STRING} =~ /tomssearch/i )
{ 
	$referengine = 'TomsSearch';
	$suffix = "search";
}

##########################################################################################
my @months = ("January","February","March","April","May","June","July",
              "August","September","October","November","December");
my @days = ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
my $curdate = "$days[$wday], $mday $months[$mon] $year";
my $cdate = sprintf("%04d%02d%02d",$year,$mon,$mday);
##########################################################################################
my $htmlfile = "$xmlfile";
$htmlfile =~ s/.xml//g;		# change extension
$htmlfile .= ".$css.$suffix.html";

my $generated = 0;

# does .html.browser exist?
if( -f $htmlfile
	&& $ENV{QUERY_STRING} ne "forceregen"	# always do else and regenerate if ?forceregen is used
	&& $referengine eq "Nosearch")			# or if it is a query
{
	# date on .xml > date on .html.browser?
	if( (stat("$xmlfile"))[9] > (stat("$htmlfile"))[9] )	# was source file modified since html file was generated?
	{
		unlink $htmlfile;	# delete old version

		# generate .html.browser
		system "/usr/local/bin/perl xmlwebpage.pl $xmlfile $css $referengine > $htmlfile";
		$generated = 1;
	}
	elsif ( (stat("$xmlfile"))[9] < (time() - 24*60*60) )	# generated file is over 1 day old.
	{
		unlink $htmlfile;	# delete old version
		system "/usr/local/bin/perl xmlwebpage.pl $xmlfile $css $referengine > $htmlfile";
		$generated = 1;
	}
} else
{
	# generate .html.browser
	system "/usr/local/bin/perl xmlwebpage.pl $xmlfile $css $referengine > $htmlfile";
	$generated = 1;
}

##########################################################################################
# return .html.browser

open(HTML,$htmlfile);
while(<HTML>)
{
	print $_;
}
close(HTML);

##########################################################################################
# count based on requested $xmlfile, $browser, source location, user name etc...
my %stats;
my $count;
my $file;
my $hitcount;

open(STATS, "</www/users/madmick/cgi-bin/count.dat");
# lock file
flock(STATS,LOCK_EX);
# and, in case someone appended
# while we were waiting...
# seek(STATS, 0, 2);

# read current value
while(<STATS>)
{
	chop;
	($file,$count) = split(/=/,$_);
	$stats{$file} = $count;
}
close(STATS);

# increment values
$stats{"FILE  : $xmlfile"} += 1;
my $hitcount =  $stats{"FILE  : $xmlfile"};
##########################################################################################
# Output the bottom bit of the page with the hit count...

print "<br>$hitcount <a href=\"http://yoyo.cc.monash.edu.au/~madmick/cgi-bin/showcount.pl\">hits</a> at $curdate.\n";

print "</div>\n";
print "</body></html>\n";
##########################################################################################

$stats{"TOTAL : "} += 1;
$stats{"CSS   : $css"} += 1;
$stats{"CLIENT: $realbrowser"} += 1;
$stats{"DATE  : $cdate $curdate"} += 1;

my $query = $ENV{QUERY_STRING};
$query =~ s/=/--/g;
$query =~ s/:/../g;
$stats{"QUERY : $query"} += 1;
$stats{"HOST  : $ENV{REMOTE_HOST}"} += 1;

# $refer is set above
$refer =~ s/=/--/g;		# can't have : or = in there...
$refer =~ s/:/../g;

$stats{"REFER : $refer"} += 1;
$stats{"SEARCH: $referengine"} += 1;

if($generated == 1)
{
	$stats{"GEN   : $cdate $curdate"} += 1;
}

my $kw = "(none)";

if($referengine eq 'TomsSearch')
{
	# fire up the CGI module
	my $cgi = new CGI(lc $ENV{QUERY_STRING});

	# get the value of the parameter
	$kw = $cgi->param('tomssearch');

	# remove any quotes or + signs
	$kw =~ s/"//g; $kw =~ s/\+//g;
} else
{
	# split query parameters off the end
	my ($site,$query) = split( /\?/, lc $ENV{HTTP_REFERER});

	# fire up the CGI module
	my $cgi = new CGI($query);

	# get the value of the parameter
	if( $referengine eq "Yahoo" ) { $kw = $cgi->param('p'); }
	elsif( $referengine eq "Excite" ) { $kw = $cgi->param('search'); }
	elsif( $referengine eq "Ultraseek" ) { $kw = $cgi->param('qt'); }
	elsif( $referengine eq "Altavista" ) { $kw = $cgi->param('q'); }
	elsif( $referengine eq "Anzwers" ) { $kw = $cgi->param('query'); }
	elsif( $referengine eq "Google" ) { $kw = $cgi->param('q'); }
	elsif( $referengine eq "HotBot" ) { $kw = $cgi->param('MT'); }
	elsif( $referengine eq "FireBall" ) { $kw = $cgi->param('q'); }
	elsif( $referengine eq "NineMSN" ) { $kw = $cgi->param('MT'); }
	elsif( $referengine eq "MSN" ) { $kw = $cgi->param('MT'); }
	elsif( $referengine eq "Go2Net" ) { $kw = $cgi->param('general'); }
	elsif( $referengine eq "Iltrovatore" ) { $kw = $cgi->param('what'); }
	elsif( $referengine eq "CNet" ) { $kw = $cgi->param('query'); }

	# remove any quotes or + signs
	$kw =~ s/"//g; $kw =~ s/\+//g;
}
$stats{"KEYWRD: $kw"} += 1;

##########################################################################################
# write hitcount values to file
open(STATS, ">/www/users/madmick/cgi-bin/count.dat");
foreach (keys %stats)
{
	print STATS "$_=$stats{$_}\n";
}
# unlock file
flock(STATS,LOCK_UN);

close(STATS);
##########################################################################################
