// photos_v1a.js
// (c) 2002 Tom Paton
// Javascript Photobrowser navigation functions
// if you are reading this, you may want to use it for your own purposes.  
// feel free, but please let me know: paton_tom@hotmail.com

// dependencies: 
//		di20() and FWFindImage() from the FireWorks export files must be present		

var gnThumbs = 34;	// need to set this...

// get the current position & mode from querystring if given
var qs = window.location.search;
var nPosition = parseInt(qs.substr(qs.indexOf("position=")+9));
if(typeof(nPosition) != "number" || isNaN(nPosition) || nPosition == 0) 
{ 
	// default to first image
	nPosition = 1; 
}
var nMode = parseInt(qs.substr(qs.indexOf("mode=")+5));
if(typeof(nMode) != "number" || isNaN(nMode) || nMode==0) 
{ 
	// default to index mode
	nMode = 1; 	// 1: index, 2: thumb, 3: full
}

// pad the given number into a 3 character string with leading 0's
function pad(n)
{
	var c = n.toString();
	if(n < 100) { c = "0"+c; }
	if(n < 10) { c = "0"+c; }
	return c;
}

// show the current image based on the mode
function Show()
{
	// check bounds
	if(nPosition < 1) { nPosition = 1; }
	if(nPosition > gnThumbs) { nPosition = gnThumbs; }
	
	if(nMode==1) // index
	{
		// find index containing thumbnail
		var nIndex = Math.ceil(nPosition/9);

	    var theImage = FWFindImage(document, "photos_v1a_preview", 0);
	    if (theImage) {
	        theImage.src = "index_"+pad(nIndex)+".jpg";
			theImage.useMap = "#photos_v1a_index";
	    }
		
		di20("photos_v1a_mode1","photos_v1a_mode1_F3.gif");
		di20('photos_v1a_mode2','photos_v1a_mode2.gif');
		di20("photos_v1a_mode3","photos_v1a_mode3.gif");
	}
	else
	if(nMode==2)	// single thumbnail
	{
		// just display thumbnail
	    var theImage = FWFindImage(document, "photos_v1a_preview", 0);
	    if (theImage) {
	        theImage.src = "thumb_"+pad(nPosition)+".jpg";
			theImage.useMap = "#photos_v1a_thumb";
	    }

		di20("photos_v1a_mode1","photos_v1a_mode1.gif");
		di20("photos_v1a_mode2","photos_v1a_mode2_F3.gif");
		di20("photos_v1a_mode3","photos_v1a_mode3.gif");
	}
	else
	if(nMode==3)	// full image
	{
		// display seperate page
		var fullURL = "photos_v1a_full.htm?position="+nPosition;
		window.location.href = fullURL;

		//di20("photos_v1a_mode1","photos_v1a_mode1.gif");
		//di20("photos_v1a_mode2","photos_v1a_mode2.gif");
		//di20("photos_v1a_mode3","photos_v1a_mode3_F3.gif");
	}
}

// navigate to first thumbnail/index page
function GoFirst()
{
	nPosition = 1;
	Show();
}

// skip back one thumbnail/index page
function GoPrevious()
{
	if(nMode==1) { nPosition -= 9; }	// 9 thumbs per index page
	else 
	if(nMode==2 
	|| nMode==3) { --nPosition; }
	
	Show();
}

// skip forward one thumbnail/index page
function GoNext()
{
	if(nMode==1) { nPosition += 9; }	// 9 thumbs per index page
	else 
	if(nMode==2 
	|| nMode==3) { ++nPosition; }

	Show();
}

// navigate to last thumbnail/index page
function GoLast()
{
	nPosition = gnThumbs;
	Show();
}

// go to index mode
function ViewIndex()
{
	if(nMode != 1)
	{
		nMode = 1;
		Show();
	}
}

// go to thumbnail view mode
// pnOffset is thumbnail position relative to start of current page
function ViewThumb(pnOffset)
{
	if(nMode != 2)
	{
		// default to first one
		if(typeof(pnOffset) != "number") {pnOffset = 1;}
	
		// figure out what page we're on
		// the go to an image relative to that
		nPosition = Math.floor(nPosition/9)*9 + pnOffset;
		nMode = 2;
		Show();
	}
}

function ZoomIn()
{
	if(nMode == 1)
	{
		// default to first one
		if(typeof(pnOffset) != "number") {pnOffset = 1;}
	
		// figure out what page we're on
		// the go to an image relative to that
		nPosition = Math.floor(nPosition/9)*9 + pnOffset;
		nMode = 2;
		Show();
	}
	else if(nMode == 2)
	{
		nMode = 3;
		Show();
	}
	
	// can't click from mode 3...
}

function ZoomOut()
{
	// don't do anything if mode 1...
	
	if(nMode == 2)
	{
		nMode = 1;
		Show();
	}
	
	// can't click this from mode 3...
}

// go to full size mode
function ViewFullSize()
{
	if(nMode != 3)
	{
		nMode = 3;
		Show();
	}
}

// handle click on index image or thumbnail
function GoClick(pnOffset)
{
	GoOut();	// mouseout

	if(nMode==1)
	{
		// index, go to thumb
		ViewThumb(pnOffset);
	}
	else
	if(nMode==2)
	{
		// thumb, go to full
		ViewFullSize();
	}
}

// onmouseout for index image/thumbnail
function GoOut()
{
	if(nMode==1)
	{
		di20('photos_v1a_mode2','photos_v1a_mode2.gif');
	}
	else
	if(nMode==2)
	{
		di20('photos_v1a_full','photos_v1a_full.gif');
		di20('photos_v1a_mode3','photos_v1a_mode3.gif');
	}
	di20('photos_v1a_thumb','photos_v1a_thumb.gif');
}
// onmouseover for index image/thumbnail
function GoOver()
{
	if(nMode==1)
	{
		di20('photos_v1a_mode2','photos_v1a_mode2_F2.gif');
	}
	else
	if(nMode==2)
	{
		di20('photos_v1a_full','photos_v1a_full_F2.gif');
		di20('photos_v1a_mode3','photos_v1a_mode3_F2.gif');
	}
	di20('photos_v1a_thumb','photos_v1a_thumb_F2.gif');
}
var glAboutMode = false;
function ToggleAbout()
{
    var theImage = FWFindImage(document, "photos_v1a_preview", 0);
    if (theImage) {
		if(glAboutMode)
		{
			glAboutMode = false;
			Show();
		}
		else
		{
	        theImage.src = 'photos_v1a_preview_F2.gif';
			theImage.useMap = "#photos_v1a_about";
			glAboutMode = true;
		}
    }
}

function ModeOut(pnMode)
{
	if(pnMode==1)
	{
		if(nMode==1)
		{
			di20('photos_v1a_mode1','photos_v1a_mode1_F3.gif');
		}
		else
		{
			di20('photos_v1a_mode1','photos_v1a_mode1.gif');
		}
	}
	else if(pnMode==2)
	{
		if(nMode==2)
		{
			di20('photos_v1a_mode2','photos_v1a_mode2_F3.gif');
		}
		else
		{
			di20('photos_v1a_mode2','photos_v1a_mode2.gif');
		}
	}
	else if(pnMode==3)
	{
		di20('photos_v1a_full','photos_v1a_full.gif');
		if(nMode==3)
		{
			di20('photos_v1a_mode3','photos_v1a_mode3_F3.gif');
		}
		else
		{
			di20('photos_v1a_mode3','photos_v1a_mode3.gif');
		}
	}
	if(pnMode == nMode-1)
	{
		di20('photos_v1a_index','photos_v1a_index.gif');
		di20('photos_v1a_thumb','photos_v1a_thumb.gif');
	}
	else if(pnMode == nMode+1)
	{
		di20('photos_v1a_index','photos_v1a_index.gif');
		di20('photos_v1a_thumb','photos_v1a_thumb.gif');
	}
}

function ModeOver(pnMode)
{
	if(pnMode==1)
	{
		if(nMode==1)
		{
			di20('photos_v1a_mode1','photos_v1a_mode1_F3.gif');
		}
		else
		{
			di20('photos_v1a_mode1','photos_v1a_mode1_F2.gif');
		}
	}
	else if(pnMode==2)
	{
		if(nMode==2)
		{
			di20('photos_v1a_mode2','photos_v1a_mode2_F3.gif');
		}
		else
		{
			di20('photos_v1a_mode2','photos_v1a_mode2_F2.gif');
		}
	}
	else if(pnMode==3)
	{
		di20('photos_v1a_full','photos_v1a_full_F2.gif');
		if(nMode==3)
		{
			di20('photos_v1a_mode3','photos_v1a_mode3_F3.gif');
		}
		else
		{
			di20('photos_v1a_mode3','photos_v1a_mode3_F2.gif');
		}
	}
	if(pnMode == nMode-1)
	{
		di20('photos_v1a_index','photos_v1a_index_F2.gif');
		di20('photos_v1a_thumb','photos_v1a_thumb.gif');
	}
	else if(pnMode == nMode+1)
	{
		di20('photos_v1a_index','photos_v1a_index.gif');
		di20('photos_v1a_thumb','photos_v1a_thumb_F2.gif');
	}
}
